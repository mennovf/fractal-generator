#include "buddhabrotClass.h"
#include "Program.h"
#include <iostream>

BuddhabrotClass::BuddhabrotClass(mine::Program^ tempSender, unsigned int tempSamples, int maxIts, double maxDist)
	:MandelbrotClass(maxIts, maxDist)
{
	sender = tempSender;
	samples = tempSamples;

	pixelCache = 0;
}

BuddhabrotClass::~BuddhabrotClass()
{
	delete sender;
	delete iteratedComplx;

	if (pixelCache != 0)
		clearCache();
}

void BuddhabrotClass::setSamples(unsigned int tempSamples)
{
	samples = tempSamples;
}

unsigned int BuddhabrotClass::getSamples()
{
	return samples;
}

unsigned int BuddhabrotClass::toPixel(double xy)
{
	unsigned int tempInt = static_cast<unsigned int>(xy);
	double rest = xy - tempInt;
	if(rest < 0.5)
		return tempInt;
	else
		return ++tempInt;
}

unsigned int BuddhabrotClass::pixelValueX(double x)
{
	double pixelXInDouble = ((x * sender->getZoomDivision()) + sender->getOriginX());
	return (toPixel(pixelXInDouble));
}

unsigned int BuddhabrotClass::pixelValueY(double y)
{
	double pixelYInDouble = ((y * sender->getZoomDivision()) + sender->getOriginY());
	return (toPixel(pixelYInDouble));
}

void BuddhabrotClass::buildCache()
{
	pixelCache = new unsigned int*[sender->getBMPX()];

	for (unsigned int x = 0; x < sender->getBMPX(); x++)
	{
		pixelCache[x] = new unsigned int[sender->getBMPY()];
		
		//Initialise to 0
		for(unsigned int y = 0; y < sender->getBMPY() ; y++)
			pixelCache[x][y] = 0;
	}

}

void BuddhabrotClass::clearCache()
{
	for (unsigned int i = 0 ; i < sender->getBMPX(); i++)
	{
		delete[] pixelCache[i];
	}
	delete[] pixelCache;
	pixelCache = 0;
}

unsigned int BuddhabrotClass::getPixelFromCache(unsigned int x, unsigned int y)
{

		return pixelCache[x][y];
		//return (*(*(pixelCache + x) + y));

}

void BuddhabrotClass::random()
{
	complx->random();

}


void BuddhabrotClass::iterate(bool firstIt)
{
	if(firstIt)
	{
		startComplx->setReal(complx->Real());
		startComplx->setImag(complx->Imag());
		iteratedComplx = (complx->squared())->add(startComplx);
	}
	else 
		iteratedComplx = (iteratedComplx->squared())->add(startComplx);
}

void BuddhabrotClass::calculateAndApply()
{

		for (int i = 0; i < getIts(); i++)
		{
			iterate(i == 0);

			unsigned int pixelX = pixelValueX(iteratedComplx->Real());
			unsigned int pixelY = pixelValueY(iteratedComplx->Imag());
			
			if (pixelX < sender->getBMPX() && pixelX >= 0 && pixelY < sender->getBMPY() && pixelY >= 0)
			{

				//Pointer arithmetic
					//(*(*(pixelCache + (pixelX)) + pixelY))++;
					pixelCache[pixelX][pixelY]++;

				
			}
		}
}

void BuddhabrotClass::setMax(double max)
{
	complx->setMax(max);
}

void BuddhabrotClass::setMin(double min)
{
	complx->setMin(min);
}

