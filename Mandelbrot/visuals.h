
#include "Program.h"
#include <cliext/vector>


#ifndef VISUALS_H
#define VISUALS_H

ref class Visuals
{
public:

	Visuals(mine::Program ^ prg);
	~Visuals();

	//void put_pixel(int x, int y, Uint32 pixel);
	
	//Uint32 getColor(int iteration);
	//Get Windows Colour
	System::Drawing::Color getColor(int iteration); 
	void setScheme(unsigned short tempInt);
	unsigned short getScheme();

	void setColorAmount(unsigned short tempAmount);
	void setActiveColor(unsigned short tempColor){activeColor = tempColor;}

	void setR(unsigned int location, unsigned short value );
	void setG(unsigned int location, unsigned short value );
	void setB(unsigned int location, unsigned short value );

	void setColor2Sequential(bool tempBool);

	//Junk function
	unsigned int stopLNK2022() { return reds->size() * greens->size() * blues->size(); }

protected:
	
	//0:Black>Red>Blue>Black 1:Black>Yellow>Green>White 2:Custom
	unsigned short colorScheme;
	bool color2Sequential;

	mine::Program ^ program;
	System::Drawing::Color getColor0(int iteration);
	System::Drawing::Color getColor1(int iteration);
	System::Drawing::Color getColor2(int iteration);
	System::Drawing::Color getColor2Sequential(int iteration);
	System::Drawing::Color getColor2Lineair(int iteration);

	unsigned short colorAmount;
	unsigned short activeColor;
	cliext::vector<unsigned short>^ reds;
	cliext::vector<unsigned short>^ greens;
	cliext::vector<unsigned short>^ blues;
};



#endif