#ifndef MANDELBROTCLASS_H
#define MANDELBROTCLASS_H

#include "complex.h"

public ref class MandelbrotClass
{
public:
	//(de)constructor(s) and copy constructor
	MandelbrotClass(int maxIts, double maxDist);
	~MandelbrotClass();
	MandelbrotClass(MandelbrotClass% brot);

	int calculateIts();

	void setMaxDist(double tempMaxDist);
	void setMaxIts(int tempMaxIts);
	int getMaxIts();
	double getMaxDist() {return maxDist;};

	int getIts(){return iterationsRequired;}

	void setReal(double realPart);
	void setImag(double imagPart);

	double getReal(){return complx->Real();};
	double getImag(){return complx->Imag();};

	unsigned int getSeed() {return complx->getSeed();}
	void setSeed(unsigned int seed){complx->setSeed(seed);}


protected:

	Complex^ complx;
	int maxIts;
	double maxDist;
	int iterationsRequired; 

	/*bool autoCalculate;
	bool hasCalculated;*/
protected:

	int numIts;
	Complex^ startComplx;

};






#endif