#include "Program.h"
#include <ctime>
#include "PictureFrameDialog.h"
#include "PictureTooBigForm.h"
#include "CustomScheme.h"
#include "Point To Trace.h"

//disable warning c4100 
#pragma warning(disable : 4100)
#pragma once


namespace Mandelbrot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace cli;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1()
		{
			
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			saveFDLG->Filter = "PNG (*.png)|*.png|JPEG (*.jpg)|*.jpg|Bitmap (*.BMP)|*.bmp";
			program = gcnew mine::Program(prgBar, BGWorker);
			BGWorkerIsBusy = false;
			dimensionsLocked = true;
			cmbBox_colorScheme->SelectedIndex = 0;
			schemeForm = gcnew CustomScheme(program);

			rdioButton_Mandelbrot->Checked = true;
			spnr_JuliaReal->Enabled = false;
			spnr_JuliaImaginary->Enabled = false;
			btn_customColorScheme->Enabled = false;


		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
			delete program;
		}

	private: mine::Program^ program;
	protected: bool BGWorkerIsBusy;
			   bool dimensionsLocked;
			   CustomScheme^ schemeForm;

	private: System::Windows::Forms::GroupBox^  OriginGroup;
	private: System::Windows::Forms::GroupBox^  groupBMP;


	private: System::Windows::Forms::Label^  lbl_OriginY;
	private: System::Windows::Forms::Label^  lbl_OriginX;
	private: System::Windows::Forms::TextBox^  txtBox_Path;
	private: System::Windows::Forms::Label^  lbl_Path;
	private: System::Windows::Forms::GroupBox^  groupBMPSize;
	private: System::Windows::Forms::Label^  lbl_SizeY;
	private: System::Windows::Forms::Label^  lbl_sizeX;


	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::NumericUpDown^  spnr_OriginY;
	private: System::Windows::Forms::NumericUpDown^  spnr_OriginX;
	private: System::Windows::Forms::NumericUpDown^  spnr_SizeY;
	private: System::Windows::Forms::NumericUpDown^  spnr_SizeX;
	private: System::Windows::Forms::GroupBox^  grpBox_Mandelbrot;
	private: System::Windows::Forms::NumericUpDown^  spnr_MaxDist;
	private: System::Windows::Forms::NumericUpDown^  spnr_MaxIts;
	private: System::Windows::Forms::Label^  lbl_MaxDist;
	private: System::Windows::Forms::Label^  lbl_MaxIts;
	private: System::Windows::Forms::Button^  btn_Create2;

	private: System::Windows::Forms::Label^  label1;


	private: System::Windows::Forms::Label^  lbl_Zoom;
	private: System::Windows::Forms::NumericUpDown^  spnr_Zoom;
	private: System::Windows::Forms::ProgressBar^  prgBar;


	private: System::Windows::Forms::GroupBox^  grpBox_FractalType;
	private: System::Windows::Forms::RadioButton^  rdioButton_Julia;
	private: System::Windows::Forms::RadioButton^  rdioButton_Mandelbrot;
	private: System::Windows::Forms::GroupBox^  grpBox_Julia;
	private: System::Windows::Forms::Label^  lbl_IsMandelbrot;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::NumericUpDown^  spnr_JuliaImaginary;
	private: System::Windows::Forms::Label^  lbl_JuliaReal;
	private: System::Windows::Forms::NumericUpDown^  spnr_JuliaReal;
	private: System::ComponentModel::BackgroundWorker^  BGWorker;

	private: System::Windows::Forms::Label^  lbl_Time;
	private: System::Windows::Forms::RadioButton^  rdioButton_Buddhabrot;
	private: System::Windows::Forms::NumericUpDown^  spnr_Samples;
	private: System::Windows::Forms::Label^  lbl_Samples;
	private: System::ComponentModel::BackgroundWorker^  BGWorker2;
	private: System::Windows::Forms::CheckBox^  chkBox_lockDimensions;
	private: System::Windows::Forms::SaveFileDialog^  saveFDLG;
	private: System::Windows::Forms::Button^  btn_Save;
	private: System::Windows::Forms::GroupBox^  grpBox_Colour;
	private: System::Windows::Forms::ComboBox^  cmbBox_colorScheme;
	private: System::Windows::Forms::Button^  btn_customColorScheme;
	private: System::Windows::Forms::Button^  btn_Trace;





	private: System::ComponentModel::IContainer^  components;












	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent()
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->OriginGroup = (gcnew System::Windows::Forms::GroupBox());
			this->spnr_OriginY = (gcnew System::Windows::Forms::NumericUpDown());
			this->spnr_OriginX = (gcnew System::Windows::Forms::NumericUpDown());
			this->lbl_OriginY = (gcnew System::Windows::Forms::Label());
			this->lbl_OriginX = (gcnew System::Windows::Forms::Label());
			this->groupBMP = (gcnew System::Windows::Forms::GroupBox());
			this->btn_Save = (gcnew System::Windows::Forms::Button());
			this->spnr_Zoom = (gcnew System::Windows::Forms::NumericUpDown());
			this->lbl_Zoom = (gcnew System::Windows::Forms::Label());
			this->txtBox_Path = (gcnew System::Windows::Forms::TextBox());
			this->lbl_Path = (gcnew System::Windows::Forms::Label());
			this->groupBMPSize = (gcnew System::Windows::Forms::GroupBox());
			this->chkBox_lockDimensions = (gcnew System::Windows::Forms::CheckBox());
			this->spnr_SizeY = (gcnew System::Windows::Forms::NumericUpDown());
			this->spnr_SizeX = (gcnew System::Windows::Forms::NumericUpDown());
			this->lbl_SizeY = (gcnew System::Windows::Forms::Label());
			this->lbl_sizeX = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->grpBox_Mandelbrot = (gcnew System::Windows::Forms::GroupBox());
			this->spnr_Samples = (gcnew System::Windows::Forms::NumericUpDown());
			this->lbl_Samples = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->spnr_MaxDist = (gcnew System::Windows::Forms::NumericUpDown());
			this->spnr_MaxIts = (gcnew System::Windows::Forms::NumericUpDown());
			this->lbl_MaxDist = (gcnew System::Windows::Forms::Label());
			this->lbl_MaxIts = (gcnew System::Windows::Forms::Label());
			this->btn_Create2 = (gcnew System::Windows::Forms::Button());
			this->prgBar = (gcnew System::Windows::Forms::ProgressBar());
			this->grpBox_FractalType = (gcnew System::Windows::Forms::GroupBox());
			this->rdioButton_Buddhabrot = (gcnew System::Windows::Forms::RadioButton());
			this->rdioButton_Julia = (gcnew System::Windows::Forms::RadioButton());
			this->rdioButton_Mandelbrot = (gcnew System::Windows::Forms::RadioButton());
			this->grpBox_Julia = (gcnew System::Windows::Forms::GroupBox());
			this->lbl_IsMandelbrot = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->spnr_JuliaImaginary = (gcnew System::Windows::Forms::NumericUpDown());
			this->lbl_JuliaReal = (gcnew System::Windows::Forms::Label());
			this->spnr_JuliaReal = (gcnew System::Windows::Forms::NumericUpDown());
			this->BGWorker = (gcnew System::ComponentModel::BackgroundWorker());
			this->lbl_Time = (gcnew System::Windows::Forms::Label());
			this->BGWorker2 = (gcnew System::ComponentModel::BackgroundWorker());
			this->saveFDLG = (gcnew System::Windows::Forms::SaveFileDialog());
			this->grpBox_Colour = (gcnew System::Windows::Forms::GroupBox());
			this->btn_customColorScheme = (gcnew System::Windows::Forms::Button());
			this->cmbBox_colorScheme = (gcnew System::Windows::Forms::ComboBox());
			this->btn_Trace = (gcnew System::Windows::Forms::Button());
			this->OriginGroup->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_OriginY))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_OriginX))->BeginInit();
			this->groupBMP->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_Zoom))->BeginInit();
			this->groupBMPSize->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_SizeY))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_SizeX))->BeginInit();
			this->grpBox_Mandelbrot->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_Samples))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_MaxDist))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_MaxIts))->BeginInit();
			this->grpBox_FractalType->SuspendLayout();
			this->grpBox_Julia->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_JuliaImaginary))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_JuliaReal))->BeginInit();
			this->grpBox_Colour->SuspendLayout();
			this->SuspendLayout();
			// 
			// OriginGroup
			// 
			this->OriginGroup->Controls->Add(this->spnr_OriginY);
			this->OriginGroup->Controls->Add(this->spnr_OriginX);
			this->OriginGroup->Controls->Add(this->lbl_OriginY);
			this->OriginGroup->Controls->Add(this->lbl_OriginX);
			this->OriginGroup->Location = System::Drawing::Point(12, 175);
			this->OriginGroup->Name = L"OriginGroup";
			this->OriginGroup->Size = System::Drawing::Size(320, 63);
			this->OriginGroup->TabIndex = 1;
			this->OriginGroup->TabStop = false;
			this->OriginGroup->Text = L"Origin";
			// 
			// spnr_OriginY
			// 
			this->spnr_OriginY->DecimalPlaces = 2;
			this->spnr_OriginY->Location = System::Drawing::Point(188, 23);
			this->spnr_OriginY->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {-1593835521, 466537709, 54210, 0});
			this->spnr_OriginY->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {268435455, 1042612833, 542101086, System::Int32::MinValue});
			this->spnr_OriginY->Name = L"spnr_OriginY";
			this->spnr_OriginY->Size = System::Drawing::Size(110, 20);
			this->spnr_OriginY->TabIndex = 6;
			this->spnr_OriginY->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_OriginY->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {500, 0, 0, 65536});
			this->spnr_OriginY->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_OriginY_ValueChanged);
			// 
			// spnr_OriginX
			// 
			this->spnr_OriginX->DecimalPlaces = 2;
			this->spnr_OriginX->Location = System::Drawing::Point(27, 23);
			this->spnr_OriginX->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {-1593835521, 466537709, 54210, 0});
			this->spnr_OriginX->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {268435455, 1042612833, 542101086, System::Int32::MinValue});
			this->spnr_OriginX->Name = L"spnr_OriginX";
			this->spnr_OriginX->Size = System::Drawing::Size(110, 20);
			this->spnr_OriginX->TabIndex = 5;
			this->spnr_OriginX->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_OriginX->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {750, 0, 0, 65536});
			this->spnr_OriginX->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_OriginX_ValueChanged);
			// 
			// lbl_OriginY
			// 
			this->lbl_OriginY->AutoSize = true;
			this->lbl_OriginY->Location = System::Drawing::Point(162, 25);
			this->lbl_OriginY->Name = L"lbl_OriginY";
			this->lbl_OriginY->Size = System::Drawing::Size(25, 13);
			this->lbl_OriginY->TabIndex = 1;
			this->lbl_OriginY->Text = L"Y%:";
			// 
			// lbl_OriginX
			// 
			this->lbl_OriginX->AutoSize = true;
			this->lbl_OriginX->Location = System::Drawing::Point(0, 25);
			this->lbl_OriginX->Name = L"lbl_OriginX";
			this->lbl_OriginX->Size = System::Drawing::Size(25, 13);
			this->lbl_OriginX->TabIndex = 0;
			this->lbl_OriginX->Text = L"X%:";
			// 
			// groupBMP
			// 
			this->groupBMP->Controls->Add(this->btn_Save);
			this->groupBMP->Controls->Add(this->spnr_Zoom);
			this->groupBMP->Controls->Add(this->lbl_Zoom);
			this->groupBMP->Controls->Add(this->txtBox_Path);
			this->groupBMP->Controls->Add(this->lbl_Path);
			this->groupBMP->Controls->Add(this->groupBMPSize);
			this->groupBMP->Controls->Add(this->textBox1);
			this->groupBMP->Location = System::Drawing::Point(12, 12);
			this->groupBMP->Name = L"groupBMP";
			this->groupBMP->Size = System::Drawing::Size(320, 157);
			this->groupBMP->TabIndex = 2;
			this->groupBMP->TabStop = false;
			this->groupBMP->Text = L"Bitmap Options";
			// 
			// btn_Save
			// 
			this->btn_Save->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btn_Save->Location = System::Drawing::Point(274, 126);
			this->btn_Save->Name = L"btn_Save";
			this->btn_Save->Size = System::Drawing::Size(29, 23);
			this->btn_Save->TabIndex = 7;
			this->btn_Save->Text = L"...";
			this->btn_Save->UseVisualStyleBackColor = true;
			this->btn_Save->Click += gcnew System::EventHandler(this, &Form1::btn_Save_Click);
			// 
			// spnr_Zoom
			// 
			this->spnr_Zoom->DecimalPlaces = 2;
			this->spnr_Zoom->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 65536});
			this->spnr_Zoom->Location = System::Drawing::Point(213, 89);
			this->spnr_Zoom->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {1410065407, 2, 0, 0});
			this->spnr_Zoom->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 131072});
			this->spnr_Zoom->Name = L"spnr_Zoom";
			this->spnr_Zoom->Size = System::Drawing::Size(90, 20);
			this->spnr_Zoom->TabIndex = 3;
			this->spnr_Zoom->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_Zoom->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {7, 0, 0, 65536});
			this->spnr_Zoom->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_Zoom_ValueChanged);
			// 
			// lbl_Zoom
			// 
			this->lbl_Zoom->AutoSize = true;
			this->lbl_Zoom->Location = System::Drawing::Point(10, 89);
			this->lbl_Zoom->Name = L"lbl_Zoom";
			this->lbl_Zoom->Size = System::Drawing::Size(138, 13);
			this->lbl_Zoom->TabIndex = 3;
			this->lbl_Zoom->Text = L"Zoom (Increase to zoom in):";
			// 
			// txtBox_Path
			// 
			this->txtBox_Path->Location = System::Drawing::Point(60, 128);
			this->txtBox_Path->Name = L"txtBox_Path";
			this->txtBox_Path->Size = System::Drawing::Size(205, 20);
			this->txtBox_Path->TabIndex = 4;
			this->txtBox_Path->TextChanged += gcnew System::EventHandler(this, &Form1::txtBox_Path_TextChanged);
			// 
			// lbl_Path
			// 
			this->lbl_Path->AutoSize = true;
			this->lbl_Path->Location = System::Drawing::Point(7, 131);
			this->lbl_Path->Name = L"lbl_Path";
			this->lbl_Path->Size = System::Drawing::Size(32, 13);
			this->lbl_Path->TabIndex = 2;
			this->lbl_Path->Text = L"Path:";
			// 
			// groupBMPSize
			// 
			this->groupBMPSize->Controls->Add(this->chkBox_lockDimensions);
			this->groupBMPSize->Controls->Add(this->spnr_SizeY);
			this->groupBMPSize->Controls->Add(this->spnr_SizeX);
			this->groupBMPSize->Controls->Add(this->lbl_SizeY);
			this->groupBMPSize->Controls->Add(this->lbl_sizeX);
			this->groupBMPSize->Location = System::Drawing::Point(6, 19);
			this->groupBMPSize->Name = L"groupBMPSize";
			this->groupBMPSize->Size = System::Drawing::Size(308, 47);
			this->groupBMPSize->TabIndex = 1;
			this->groupBMPSize->TabStop = false;
			this->groupBMPSize->Text = L"Size";
			// 
			// chkBox_lockDimensions
			// 
			this->chkBox_lockDimensions->AutoSize = true;
			this->chkBox_lockDimensions->Checked = true;
			this->chkBox_lockDimensions->CheckState = System::Windows::Forms::CheckState::Checked;
			this->chkBox_lockDimensions->Location = System::Drawing::Point(135, 18);
			this->chkBox_lockDimensions->Name = L"chkBox_lockDimensions";
			this->chkBox_lockDimensions->Size = System::Drawing::Size(50, 17);
			this->chkBox_lockDimensions->TabIndex = 4;
			this->chkBox_lockDimensions->Text = L"Lock";
			this->chkBox_lockDimensions->UseVisualStyleBackColor = true;
			this->chkBox_lockDimensions->CheckedChanged += gcnew System::EventHandler(this, &Form1::chkBox_lockDimensions_CheckedChanged);
			// 
			// spnr_SizeY
			// 
			this->spnr_SizeY->Enabled = false;
			this->spnr_SizeY->Location = System::Drawing::Point(206, 17);
			this->spnr_SizeY->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {99999999, 0, 0, 0});
			this->spnr_SizeY->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 0});
			this->spnr_SizeY->Name = L"spnr_SizeY";
			this->spnr_SizeY->Size = System::Drawing::Size(90, 20);
			this->spnr_SizeY->TabIndex = 1;
			this->spnr_SizeY->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_SizeY->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {1500, 0, 0, 0});
			this->spnr_SizeY->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_SizeY_ValueChanged);
			// 
			// spnr_SizeX
			// 
			this->spnr_SizeX->Location = System::Drawing::Point(40, 17);
			this->spnr_SizeX->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {9999999, 0, 0, 0});
			this->spnr_SizeX->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 0});
			this->spnr_SizeX->Name = L"spnr_SizeX";
			this->spnr_SizeX->Size = System::Drawing::Size(90, 20);
			this->spnr_SizeX->TabIndex = 0;
			this->spnr_SizeX->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_SizeX->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {1500, 0, 0, 0});
			this->spnr_SizeX->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_SizeX_ValueChanged);
			// 
			// lbl_SizeY
			// 
			this->lbl_SizeY->AutoSize = true;
			this->lbl_SizeY->Location = System::Drawing::Point(183, 19);
			this->lbl_SizeY->Name = L"lbl_SizeY";
			this->lbl_SizeY->Size = System::Drawing::Size(17, 13);
			this->lbl_SizeY->TabIndex = 3;
			this->lbl_SizeY->Text = L"Y:";
			// 
			// lbl_sizeX
			// 
			this->lbl_sizeX->AutoSize = true;
			this->lbl_sizeX->Location = System::Drawing::Point(17, 19);
			this->lbl_sizeX->Name = L"lbl_sizeX";
			this->lbl_sizeX->Size = System::Drawing::Size(17, 13);
			this->lbl_sizeX->TabIndex = 2;
			this->lbl_sizeX->Text = L"X:";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(32, 35);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(40, 20);
			this->textBox1->TabIndex = 0;
			// 
			// grpBox_Mandelbrot
			// 
			this->grpBox_Mandelbrot->Controls->Add(this->spnr_Samples);
			this->grpBox_Mandelbrot->Controls->Add(this->lbl_Samples);
			this->grpBox_Mandelbrot->Controls->Add(this->label1);
			this->grpBox_Mandelbrot->Controls->Add(this->spnr_MaxDist);
			this->grpBox_Mandelbrot->Controls->Add(this->spnr_MaxIts);
			this->grpBox_Mandelbrot->Controls->Add(this->lbl_MaxDist);
			this->grpBox_Mandelbrot->Controls->Add(this->lbl_MaxIts);
			this->grpBox_Mandelbrot->Location = System::Drawing::Point(12, 380);
			this->grpBox_Mandelbrot->Name = L"grpBox_Mandelbrot";
			this->grpBox_Mandelbrot->Size = System::Drawing::Size(320, 100);
			this->grpBox_Mandelbrot->TabIndex = 3;
			this->grpBox_Mandelbrot->TabStop = false;
			this->grpBox_Mandelbrot->Text = L"Mandelbrot";
			// 
			// spnr_Samples
			// 
			this->spnr_Samples->Location = System::Drawing::Point(60, 73);
			this->spnr_Samples->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {999999999, 0, 0, 0});
			this->spnr_Samples->Name = L"spnr_Samples";
			this->spnr_Samples->Size = System::Drawing::Size(88, 20);
			this->spnr_Samples->TabIndex = 10;
			this->spnr_Samples->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_Samples->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {10000000, 0, 0, 0});
			this->spnr_Samples->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_Samples_ValueChanged);
			// 
			// lbl_Samples
			// 
			this->lbl_Samples->AutoSize = true;
			this->lbl_Samples->Location = System::Drawing::Point(10, 75);
			this->lbl_Samples->Name = L"lbl_Samples";
			this->lbl_Samples->Size = System::Drawing::Size(53, 13);
			this->lbl_Samples->TabIndex = 9;
			this->lbl_Samples->Text = L"Samples: ";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(142, 20);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(123, 13);
			this->label1->TabIndex = 7;
			this->label1->Text = L"(Decrease for less detail)";
			// 
			// spnr_MaxDist
			// 
			this->spnr_MaxDist->DecimalPlaces = 2;
			this->spnr_MaxDist->Location = System::Drawing::Point(145, 41);
			this->spnr_MaxDist->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {99999, 0, 0, 0});
			this->spnr_MaxDist->Name = L"spnr_MaxDist";
			this->spnr_MaxDist->Size = System::Drawing::Size(113, 20);
			this->spnr_MaxDist->TabIndex = 8;
			this->spnr_MaxDist->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_MaxDist->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {2, 0, 0, 0});
			this->spnr_MaxDist->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_MaxDist_ValueChanged);
			// 
			// spnr_MaxIts
			// 
			this->spnr_MaxIts->Location = System::Drawing::Point(10, 41);
			this->spnr_MaxIts->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {1569325055, 23283064, 0, 0});
			this->spnr_MaxIts->Name = L"spnr_MaxIts";
			this->spnr_MaxIts->Size = System::Drawing::Size(113, 20);
			this->spnr_MaxIts->TabIndex = 7;
			this->spnr_MaxIts->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_MaxIts->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) {50, 0, 0, 0});
			this->spnr_MaxIts->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_MaxIts_ValueChanged);
			// 
			// lbl_MaxDist
			// 
			this->lbl_MaxDist->AutoSize = true;
			this->lbl_MaxDist->Location = System::Drawing::Point(145, 7);
			this->lbl_MaxDist->Name = L"lbl_MaxDist";
			this->lbl_MaxDist->Size = System::Drawing::Size(99, 13);
			this->lbl_MaxDist->TabIndex = 1;
			this->lbl_MaxDist->Text = L"Maximum Distance:";
			// 
			// lbl_MaxIts
			// 
			this->lbl_MaxIts->AutoSize = true;
			this->lbl_MaxIts->Location = System::Drawing::Point(7, 25);
			this->lbl_MaxIts->Name = L"lbl_MaxIts";
			this->lbl_MaxIts->Size = System::Drawing::Size(100, 13);
			this->lbl_MaxIts->TabIndex = 0;
			this->lbl_MaxIts->Text = L"Maximum Iterations:";
			// 
			// btn_Create2
			// 
			this->btn_Create2->Location = System::Drawing::Point(12, 581);
			this->btn_Create2->Name = L"btn_Create2";
			this->btn_Create2->Size = System::Drawing::Size(320, 60);
			this->btn_Create2->TabIndex = 7;
			this->btn_Create2->Text = L"Create Fractal";
			this->btn_Create2->UseVisualStyleBackColor = true;
			this->btn_Create2->Click += gcnew System::EventHandler(this, &Form1::btn_Create2_Click);
			// 
			// prgBar
			// 
			this->prgBar->Location = System::Drawing::Point(12, 680);
			this->prgBar->Name = L"prgBar";
			this->prgBar->Size = System::Drawing::Size(320, 28);
			this->prgBar->TabIndex = 9;
			// 
			// grpBox_FractalType
			// 
			this->grpBox_FractalType->Controls->Add(this->rdioButton_Buddhabrot);
			this->grpBox_FractalType->Controls->Add(this->rdioButton_Julia);
			this->grpBox_FractalType->Controls->Add(this->rdioButton_Mandelbrot);
			this->grpBox_FractalType->Location = System::Drawing::Point(12, 322);
			this->grpBox_FractalType->Name = L"grpBox_FractalType";
			this->grpBox_FractalType->Size = System::Drawing::Size(320, 52);
			this->grpBox_FractalType->TabIndex = 10;
			this->grpBox_FractalType->TabStop = false;
			this->grpBox_FractalType->Text = L"Fractal:";
			// 
			// rdioButton_Buddhabrot
			// 
			this->rdioButton_Buddhabrot->AutoSize = true;
			this->rdioButton_Buddhabrot->Location = System::Drawing::Point(91, 20);
			this->rdioButton_Buddhabrot->Name = L"rdioButton_Buddhabrot";
			this->rdioButton_Buddhabrot->Size = System::Drawing::Size(80, 17);
			this->rdioButton_Buddhabrot->TabIndex = 2;
			this->rdioButton_Buddhabrot->TabStop = true;
			this->rdioButton_Buddhabrot->Text = L"Buddhabrot";
			this->rdioButton_Buddhabrot->UseVisualStyleBackColor = true;
			this->rdioButton_Buddhabrot->CheckedChanged += gcnew System::EventHandler(this, &Form1::rdioButton_Buddhabrot_CheckedChanged);
			// 
			// rdioButton_Julia
			// 
			this->rdioButton_Julia->AutoSize = true;
			this->rdioButton_Julia->Location = System::Drawing::Point(182, 20);
			this->rdioButton_Julia->Name = L"rdioButton_Julia";
			this->rdioButton_Julia->Size = System::Drawing::Size(62, 17);
			this->rdioButton_Julia->TabIndex = 1;
			this->rdioButton_Julia->TabStop = true;
			this->rdioButton_Julia->Text = L"JuliaSet";
			this->rdioButton_Julia->UseVisualStyleBackColor = true;
			this->rdioButton_Julia->CheckedChanged += gcnew System::EventHandler(this, &Form1::rdioButton_Julia_CheckedChanged);
			// 
			// rdioButton_Mandelbrot
			// 
			this->rdioButton_Mandelbrot->AutoSize = true;
			this->rdioButton_Mandelbrot->Location = System::Drawing::Point(10, 20);
			this->rdioButton_Mandelbrot->Name = L"rdioButton_Mandelbrot";
			this->rdioButton_Mandelbrot->Size = System::Drawing::Size(78, 17);
			this->rdioButton_Mandelbrot->TabIndex = 0;
			this->rdioButton_Mandelbrot->TabStop = true;
			this->rdioButton_Mandelbrot->Text = L"Mandelbrot";
			this->rdioButton_Mandelbrot->UseVisualStyleBackColor = true;
			this->rdioButton_Mandelbrot->CheckedChanged += gcnew System::EventHandler(this, &Form1::rdioButton_Mandelbrot_CheckedChanged);
			// 
			// grpBox_Julia
			// 
			this->grpBox_Julia->Controls->Add(this->lbl_IsMandelbrot);
			this->grpBox_Julia->Controls->Add(this->label3);
			this->grpBox_Julia->Controls->Add(this->spnr_JuliaImaginary);
			this->grpBox_Julia->Controls->Add(this->lbl_JuliaReal);
			this->grpBox_Julia->Controls->Add(this->spnr_JuliaReal);
			this->grpBox_Julia->Location = System::Drawing::Point(12, 487);
			this->grpBox_Julia->Name = L"grpBox_Julia";
			this->grpBox_Julia->Size = System::Drawing::Size(320, 88);
			this->grpBox_Julia->TabIndex = 11;
			this->grpBox_Julia->TabStop = false;
			this->grpBox_Julia->Text = L"Julia";
			// 
			// lbl_IsMandelbrot
			// 
			this->lbl_IsMandelbrot->AutoSize = true;
			this->lbl_IsMandelbrot->Location = System::Drawing::Point(10, 62);
			this->lbl_IsMandelbrot->Name = L"lbl_IsMandelbrot";
			this->lbl_IsMandelbrot->Size = System::Drawing::Size(274, 13);
			this->lbl_IsMandelbrot->TabIndex = 4;
			this->lbl_IsMandelbrot->Text = L"Change the values to see if it\'s part of the mandelbrot set";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(145, 32);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(58, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Imaginary: ";
			// 
			// spnr_JuliaImaginary
			// 
			this->spnr_JuliaImaginary->DecimalPlaces = 4;
			this->spnr_JuliaImaginary->Enabled = false;
			this->spnr_JuliaImaginary->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 131072});
			this->spnr_JuliaImaginary->Location = System::Drawing::Point(217, 30);
			this->spnr_JuliaImaginary->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {99999, 0, 0, 0});
			this->spnr_JuliaImaginary->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {99999, 0, 0, System::Int32::MinValue});
			this->spnr_JuliaImaginary->Name = L"spnr_JuliaImaginary";
			this->spnr_JuliaImaginary->Size = System::Drawing::Size(73, 20);
			this->spnr_JuliaImaginary->TabIndex = 10;
			this->spnr_JuliaImaginary->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_JuliaImaginary->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_JuliaImaginary_ValueChanged);
			// 
			// lbl_JuliaReal
			// 
			this->lbl_JuliaReal->AutoSize = true;
			this->lbl_JuliaReal->Location = System::Drawing::Point(7, 32);
			this->lbl_JuliaReal->Name = L"lbl_JuliaReal";
			this->lbl_JuliaReal->Size = System::Drawing::Size(32, 13);
			this->lbl_JuliaReal->TabIndex = 1;
			this->lbl_JuliaReal->Text = L"Real:";
			// 
			// spnr_JuliaReal
			// 
			this->spnr_JuliaReal->DecimalPlaces = 4;
			this->spnr_JuliaReal->Enabled = false;
			this->spnr_JuliaReal->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 131072});
			this->spnr_JuliaReal->Location = System::Drawing::Point(60, 30);
			this->spnr_JuliaReal->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {99999, 0, 0, 0});
			this->spnr_JuliaReal->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {99999, 0, 0, System::Int32::MinValue});
			this->spnr_JuliaReal->Name = L"spnr_JuliaReal";
			this->spnr_JuliaReal->Size = System::Drawing::Size(73, 20);
			this->spnr_JuliaReal->TabIndex = 9;
			this->spnr_JuliaReal->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_JuliaReal->ValueChanged += gcnew System::EventHandler(this, &Form1::spnr_JuliaReal_ValueChanged);
			// 
			// BGWorker
			// 
			this->BGWorker->WorkerReportsProgress = true;
			this->BGWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Form1::BGWorker_DoWork);
			this->BGWorker->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &Form1::BGWorker_ProgressChanged);
			this->BGWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &Form1::BGWorker_RunWorkerCompleted);
			// 
			// lbl_Time
			// 
			this->lbl_Time->AutoSize = true;
			this->lbl_Time->Location = System::Drawing::Point(9, 644);
			this->lbl_Time->Name = L"lbl_Time";
			this->lbl_Time->Size = System::Drawing::Size(28, 13);
			this->lbl_Time->TabIndex = 12;
			this->lbl_Time->Text = L"0.00";
			this->lbl_Time->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// BGWorker2
			// 
			this->BGWorker2->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Form1::BGWorker2_DoWork);
			// 
			// saveFDLG
			// 
			this->saveFDLG->Title = L"Select a location to save";
			this->saveFDLG->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::saveFDLG_FileOk);
			// 
			// grpBox_Colour
			// 
			this->grpBox_Colour->Controls->Add(this->btn_customColorScheme);
			this->grpBox_Colour->Controls->Add(this->cmbBox_colorScheme);
			this->grpBox_Colour->Location = System::Drawing::Point(12, 244);
			this->grpBox_Colour->Name = L"grpBox_Colour";
			this->grpBox_Colour->Size = System::Drawing::Size(319, 72);
			this->grpBox_Colour->TabIndex = 13;
			this->grpBox_Colour->TabStop = false;
			this->grpBox_Colour->Text = L"Colour";
			// 
			// btn_customColorScheme
			// 
			this->btn_customColorScheme->Enabled = false;
			this->btn_customColorScheme->Location = System::Drawing::Point(212, 28);
			this->btn_customColorScheme->Name = L"btn_customColorScheme";
			this->btn_customColorScheme->Size = System::Drawing::Size(91, 21);
			this->btn_customColorScheme->TabIndex = 2;
			this->btn_customColorScheme->Text = L"Custom...";
			this->btn_customColorScheme->UseVisualStyleBackColor = true;
			this->btn_customColorScheme->Click += gcnew System::EventHandler(this, &Form1::btn_customColorScheme_Click);
			// 
			// cmbBox_colorScheme
			// 
			this->cmbBox_colorScheme->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->cmbBox_colorScheme->FormattingEnabled = true;
			this->cmbBox_colorScheme->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"Black>Red>Blue>Black", L"Black>Yellow>Green>White", 
				L"Custom..."});
			this->cmbBox_colorScheme->Location = System::Drawing::Point(13, 28);
			this->cmbBox_colorScheme->Name = L"cmbBox_colorScheme";
			this->cmbBox_colorScheme->Size = System::Drawing::Size(178, 21);
			this->cmbBox_colorScheme->TabIndex = 1;
			this->cmbBox_colorScheme->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::cmbBox_colorScheme_SelectedIndexChanged);
			// 
			// btn_Trace
			// 
			this->btn_Trace->Location = System::Drawing::Point(103, 647);
			this->btn_Trace->Name = L"btn_Trace";
			this->btn_Trace->Size = System::Drawing::Size(136, 27);
			this->btn_Trace->TabIndex = 14;
			this->btn_Trace->Text = L"Trace a point";
			this->btn_Trace->UseVisualStyleBackColor = true;
			this->btn_Trace->Click += gcnew System::EventHandler(this, &Form1::btn_Trace_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(348, 720);
			this->Controls->Add(this->btn_Trace);
			this->Controls->Add(this->grpBox_Colour);
			this->Controls->Add(this->grpBox_FractalType);
			this->Controls->Add(this->lbl_Time);
			this->Controls->Add(this->grpBox_Julia);
			this->Controls->Add(this->prgBar);
			this->Controls->Add(this->btn_Create2);
			this->Controls->Add(this->grpBox_Mandelbrot);
			this->Controls->Add(this->groupBMP);
			this->Controls->Add(this->OriginGroup);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->Name = L"Form1";
			this->Text = L"Visualize the Mandelbrot set";
			this->OriginGroup->ResumeLayout(false);
			this->OriginGroup->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_OriginY))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_OriginX))->EndInit();
			this->groupBMP->ResumeLayout(false);
			this->groupBMP->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_Zoom))->EndInit();
			this->groupBMPSize->ResumeLayout(false);
			this->groupBMPSize->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_SizeY))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_SizeX))->EndInit();
			this->grpBox_Mandelbrot->ResumeLayout(false);
			this->grpBox_Mandelbrot->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_Samples))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_MaxDist))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_MaxIts))->EndInit();
			this->grpBox_FractalType->ResumeLayout(false);
			this->grpBox_FractalType->PerformLayout();
			this->grpBox_Julia->ResumeLayout(false);
			this->grpBox_Julia->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_JuliaImaginary))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_JuliaReal))->EndInit();
			this->grpBox_Colour->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	
private: System::Void spnr_SizeX_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->setBMPSizeX((int)spnr_SizeX->Value);

			 if (chkBox_lockDimensions->Checked == true){
				 program->setBMPSizeY((unsigned int)spnr_SizeX->Value);
				 spnr_SizeY->Value = (unsigned int)spnr_SizeX->Value;
			 }
		 }

private: System::Void spnr_SizeY_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->setBMPSizeY((int)spnr_SizeY->Value);
		 }

private: System::Void spnr_OriginX_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->setOriginX((double)spnr_OriginX->Value);
		 }

private: System::Void spnr_OriginY_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 sender; e;
			 program->setOriginY((double)spnr_OriginY->Value);
		 }

private: System::Void txtBox_Path_TextChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 sender; e;
			 program->setPath(txtBox_Path->Text);
		 }

private: System::Void spnr_MaxIts_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->mandelbrot->setMaxIts((int)spnr_MaxIts->Value);
			 program->julia->setMaxIts((int)spnr_MaxIts->Value);
			 program->buddha->setMaxIts((int)spnr_MaxIts->Value);
		 }

private: System::Void spnr_MaxDist_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			program->mandelbrot->setMaxDist((double)spnr_MaxDist->Value);
			program->julia->setMaxDist((double)spnr_MaxDist->Value);
			program->buddha->setMaxDist((double)spnr_MaxDist->Value);
		 }

private: System::Void enableControls(bool enable)
		 {
			 spnr_SizeX->Enabled = enable;
			 if(!dimensionsLocked)
				spnr_SizeY->Enabled = enable;
			 if(program->getFType() != 2){
				 cmbBox_colorScheme->Enabled = enable;
				 btn_customColorScheme->Enabled = enable;
			 }

			 btn_Save->Enabled = enable;
			 txtBox_Path->Enabled = enable;
			 spnr_OriginY->Enabled = enable;
			 spnr_OriginX->Enabled = enable;
			 spnr_Zoom->Enabled = enable;
			 spnr_MaxDist->Enabled = enable;
			 spnr_MaxIts->Enabled = enable;

			 chkBox_lockDimensions->Enabled = enable;
			 
			 if(program->getFType() == 1){
				spnr_JuliaReal->Enabled = enable;
				spnr_JuliaImaginary->Enabled = enable;}

			 if(program->getFType() == 2){
				 spnr_Samples->Enabled = enable;}
		 }

private: System::Void btn_Create2_Click(System::Object^  sender, System::EventArgs^  e)  //START-------------------------------------
		 {
			 if (!BGWorkerIsBusy){
				 BGWorkerIsBusy = true;

				 enableControls(false);
				 prgBar->Value = 0;

				 program->setOriginX((double)spnr_OriginX->Value);
				 program->setOriginY((double)spnr_OriginY->Value);

				 program->setTime(0.00);

				 //Do the calculations on a diffrent thread so that the UI remains responsive
				 //Use the BG_Worker_completed event function if something has to happen, on the main thread (like UI updates), after the BG_worker 
				 BGWorker->RunWorkerAsync();
			 }
		 }

private: System::Void spnr_Zoom_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->setZoomDivision((double)spnr_Zoom->Value);
		 }


private: System::Void rdioButton_Julia_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->setFType(1);

			 cmbBox_colorScheme->Enabled = true;
			 btn_customColorScheme->Enabled = true;

			 //Set the origin to the best position for the mandelbrot
			 if (program->getOriginX() == 50.0 && program->getOriginY() == 50.0)
				 program->setOriginX(75.0);

			grpBox_Mandelbrot->Text = "Julia Set";
			spnr_Samples->Enabled = false;
			spnr_JuliaReal->Enabled = true;
			spnr_JuliaImaginary->Enabled = true;
		 }


private: System::Void rdioButton_Mandelbrot_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->setFType(0);

			 cmbBox_colorScheme->Enabled = true;
			 btn_customColorScheme->Enabled = true;

			 //Set the origin to the best position for the Julia Set
			 if(program->getOriginX() == 75.0 && program->getOriginY() == 50.0)
				 program->setOriginX(50.0);

			grpBox_Mandelbrot->Text = "Mandelbrot";
			spnr_Samples->Enabled = false;
			spnr_JuliaReal->Enabled = false;
			spnr_JuliaImaginary->Enabled = false;
		 }

private: System::Void rdioButton_Buddhabrot_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
		 {
			program->setFType(2);

			cmbBox_colorScheme->Enabled = false;
			btn_customColorScheme->Enabled = false;
			
			grpBox_Mandelbrot->Text = "Buddhabrot";
			spnr_Samples->Enabled = true;
			spnr_JuliaReal->Enabled = false;
			spnr_JuliaImaginary->Enabled = false;
		 }

private: System::Void spnr_JuliaReal_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 program->julia->setStartReal((double)spnr_JuliaReal->Value);

			 if (program->isMSet() == true)
			 {
				lbl_IsMandelbrot->Text = L"Is Part of the M-Set.";
			 }
			 else
			 {
				 lbl_IsMandelbrot->Text = L"Isn't part of the M-Set.";
			 }
		 }

private: System::Void spnr_JuliaImaginary_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->julia->setStartImag((double)spnr_JuliaImaginary->Value);

			 if (program->isMSet() == true)
			 {
				lbl_IsMandelbrot->Text = L"Is Part of the M-Set.";
			 }
			 else
			 {
				 lbl_IsMandelbrot->Text = L"Isn't part of the M-Set.";
			 }
		 }
private: System::Void BGWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) 
		 {
			 //Run the program and load the final image
			  if (program->run()){

				
			  }
			  	BGWorker->ReportProgress(100);

		 }
		 

private: System::Void BGWorker_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e) 
		 {
			 prgBar->Value = (int)e->ProgressPercentage;
		 }



private: System::Void spnr_Samples_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
		 {
			 program->buddha->setSamples((unsigned int)spnr_Samples->Value);
		 }

private: System::Void BGWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {
			 BGWorker2->RunWorkerAsync();
			 BGWorkerIsBusy = false;
			 lbl_Time->Text = (program->getTime()).ToString();
			 enableControls(true);
		 }

private: System::Void BGWorker2_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
			 if(program->getBMPX() <= 15000 && program->getBMPY() <= 15000  ){

					PictureFrameDialog^ pictureDialog = gcnew PictureFrameDialog( program->getPath(), program );
					pictureDialog->ShowDialog();
				}else{
					PictureTooBigForm^ tooBig = gcnew PictureTooBigForm();
					tooBig->ShowDialog();
			 }
			 
		 }

private: System::Void chkBox_lockDimensions_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 dimensionsLocked = chkBox_lockDimensions->Checked;

			 if (chkBox_lockDimensions->Checked == true){

				 spnr_SizeY->Enabled = false;
				 program->setBMPSizeY((unsigned int)spnr_SizeX->Value);
				 spnr_SizeY->Value = (unsigned int)spnr_SizeX->Value;
			 }else{
				 spnr_SizeY->Enabled = true;
			 }
		 }



private: System::Void btn_Save_Click(System::Object^  sender, System::EventArgs^  e) {
			 saveFDLG->ShowDialog();
		 }
private: System::Void saveFDLG_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 String^ tempFileDir = saveFDLG->FileName;
			 txtBox_Path->Text = tempFileDir;

			 if(tempFileDir->EndsWith(".bmp"))
				 program->setFileType(0);
			 else{
				 if(tempFileDir->EndsWith(".png"))
					 program->setFileType(1);
				 else{
					 if(tempFileDir->EndsWith(".jpg"))
						 program->setFileType(2);
					
				 }
			 }
		 }

private: System::Void cmbBox_colorScheme_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 program->setColorScheme((unsigned short)cmbBox_colorScheme->SelectedIndex);

			 if(cmbBox_colorScheme->SelectedIndex == 2)
				 btn_customColorScheme->Enabled = true;
			 else
				 btn_customColorScheme->Enabled = false;

		 }
private: System::Void btn_customColorScheme_Click(System::Object^  sender, System::EventArgs^  e) {
			 schemeForm->ShowDialog();
		 }
private: System::Void btn_Trace_Click(System::Object^  sender, System::EventArgs^  e) {
			 PointToTrace^ trc = gcnew PointToTrace(program);
			 trc->ShowDialog();

		 }
};
}

