#pragma once
#pragma warning (disable: 4100)

namespace Mandelbrot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for CustomColourScheme
	/// </summary>
	public ref class CustomColourScheme : public System::Windows::Forms::Form
	{
	public:
		CustomColourScheme(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~CustomColourScheme()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  lbl_colorNumber;
	protected: 
	private: System::Windows::Forms::NumericUpDown^  spnr_colorAmount;
	private: System::Windows::Forms::Label^  lbl_activeColor;
	private: System::Windows::Forms::NumericUpDown^  spnr_activeColor;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(CustomColourScheme::typeid));
			this->lbl_colorNumber = (gcnew System::Windows::Forms::Label());
			this->spnr_colorAmount = (gcnew System::Windows::Forms::NumericUpDown());
			this->lbl_activeColor = (gcnew System::Windows::Forms::Label());
			this->spnr_activeColor = (gcnew System::Windows::Forms::NumericUpDown());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_colorAmount))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_activeColor))->BeginInit();
			this->SuspendLayout();
			// 
			// lbl_colorNumber
			// 
			this->lbl_colorNumber->AutoSize = true;
			this->lbl_colorNumber->Location = System::Drawing::Point(12, 9);
			this->lbl_colorNumber->Name = L"lbl_colorNumber";
			this->lbl_colorNumber->Size = System::Drawing::Size(97, 13);
			this->lbl_colorNumber->TabIndex = 0;
			this->lbl_colorNumber->Text = L"Number of Colours:";
			// 
			// spnr_colorAmount
			// 
			this->spnr_colorAmount->Location = System::Drawing::Point(132, 7);
			this->spnr_colorAmount->Name = L"spnr_colorAmount";
			this->spnr_colorAmount->Size = System::Drawing::Size(51, 20);
			this->spnr_colorAmount->TabIndex = 1;
			this->spnr_colorAmount->ValueChanged += gcnew System::EventHandler(this, &CustomColourScheme::spnr_colorAmount_ValueChanged);
			// 
			// lbl_activeColor
			// 
			this->lbl_activeColor->AutoSize = true;
			this->lbl_activeColor->Location = System::Drawing::Point(15, 40);
			this->lbl_activeColor->Name = L"lbl_activeColor";
			this->lbl_activeColor->Size = System::Drawing::Size(73, 13);
			this->lbl_activeColor->TabIndex = 2;
			this->lbl_activeColor->Text = L"Active Colour:";
			// 
			// spnr_activeColor
			// 
			this->spnr_activeColor->Location = System::Drawing::Point(132, 40);
			this->spnr_activeColor->Name = L"spnr_activeColor";
			this->spnr_activeColor->Size = System::Drawing::Size(51, 20);
			this->spnr_activeColor->TabIndex = 3;
			// 
			// CustomColourScheme
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(249, 118);
			this->Controls->Add(this->spnr_activeColor);
			this->Controls->Add(this->lbl_activeColor);
			this->Controls->Add(this->spnr_colorAmount);
			this->Controls->Add(this->lbl_colorNumber);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->Name = L"CustomColourScheme";
			this->Text = L"Custom Colour Scheme";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_colorAmount))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_activeColor))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void spnr_colorAmount_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (spnr_activeColor->Value > spnr_colorAmount->Value)
					 spnr_activeColor->Value = spnr_colorAmount->Value;
				 spnr_activeColor->Maximum = spnr_colorAmount->Value;
			 }
};
}
