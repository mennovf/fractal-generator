#include <ctime>
#include <cstdlib>
#include "complex.h"


using namespace System;

Complex::Complex()
{
	_real = 0.0;
	_imag = 0.0;
	randSeed = static_cast<unsigned int>(time(0));
	PRNG = new MTRand_int32;
}

Complex::Complex(double real, double imag)
{
	_real = real;
	_imag = imag;
	randSeed = static_cast<unsigned int>(time(0));
	PRNG = new MTRand_int32;
}

Complex::~Complex()
{
	delete PRNG;
	PRNG = nullptr;
}

Complex::Complex(Complex% complx){
	_real = complx.Real();
	_imag = complx.Imag();
	randSeed = complx.getSeed();
	
	delete PRNG;
	PRNG = new MTRand_int32(complx.getMin(), complx.getMax());

}

	
Complex^ Complex::add(Complex^ tempComp)
{
	this->_real += tempComp->_real;
	this->_imag += tempComp->_imag;
	return this;
}



Complex^ Complex::squared()
{
	double thisReal = _real;
	double thisImag = _imag;

	_real = (thisReal * thisReal) - (thisImag * thisImag);
	_imag = 2*thisReal*thisImag;

	return this;
}

void Complex::setMin(double min)
{
	PRNG->setMin(min);
}

void Complex::setMax(double max)
{
	PRNG->setMax(max);
}