#ifndef JULIACLASS_H
#define JULIACLASS_H

#include "mandelbrotClass.h"

public ref class JuliaClass : public MandelbrotClass
{
public:

	JuliaClass(int maxIts, double maxDist);
	~JuliaClass();
	//JuliaClass(JuliaClass% julia);

	//
	int calculateIts() new;

	void setStartReal(double dReal);
	void setStartImag(double dImag);
	double getStartReal();
	double getStartImag();

protected:


};

#endif