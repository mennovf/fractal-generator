#pragma once
#include "Program.h"
#define SIZEEXTRA 30

namespace Mandelbrot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for PictureFrameDialog
	/// </summary>
	public ref class PictureFrameDialog : public System::Windows::Forms::Form
	{
	public:
		PictureFrameDialog(System::String^ path, mine::Program^ tempProg)
		{
			InitializeComponent();
			program = tempProg;
			this->ClientSize = System::Drawing::Size(XSize() + SIZEEXTRA, YSize() + SIZEEXTRA );
			//
			//TODO: Add the constructor code here
			//

			savedPath = path;

			pctrBox_MandelbrotDisplay->Size = System::Drawing::Size(XSize(), YSize());
			pctrBox_MandelbrotDisplay->Load( savedPath );

		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PictureFrameDialog()
		{
			if (components)
			{
				delete components;
			}
			delete pctrBox_MandelbrotDisplay;

		}
	private: System::Windows::Forms::PictureBox^  pctrBox_MandelbrotDisplay;
	protected: System::String^ savedPath;
	private: mine::Program^ program;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pctrBox_MandelbrotDisplay = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pctrBox_MandelbrotDisplay))->BeginInit();
			this->SuspendLayout();
			// 
			// pctrBox_MandelbrotDisplay
			// 
			this->pctrBox_MandelbrotDisplay->Location = System::Drawing::Point(13, 13);
			this->pctrBox_MandelbrotDisplay->Name = L"pctrBox_MandelbrotDisplay";
			this->pctrBox_MandelbrotDisplay->Size = System::Drawing::Size(759, 552);
			this->pctrBox_MandelbrotDisplay->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pctrBox_MandelbrotDisplay->TabIndex = 0;
			this->pctrBox_MandelbrotDisplay->TabStop = false;
			// 
			// PictureFrameDialog
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(784, 582);
			this->Controls->Add(this->pctrBox_MandelbrotDisplay);
			this->Name = L"PictureFrameDialog";
			this->Text = L"PictureFrameDialog";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pctrBox_MandelbrotDisplay))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: unsigned int XSize(){
				 unsigned int bmpX = program->getBMPX();
				 unsigned int bmpY = program->getBMPY();

				 double divisionFactor = 1.0;
				 if (bmpX < bmpY){
					 divisionFactor = (double)bmpY / 600.0;
				 }else{
					 divisionFactor = (double)bmpX / 600.0;
				 }
				 return (unsigned int)( bmpX / divisionFactor );
			 }

	private: unsigned int YSize(){
				 unsigned int bmpX = program->getBMPX();
				 unsigned int bmpY = program->getBMPY();

				 double divisionFactor = 1.0;
				 if (bmpX < bmpY){
					 divisionFactor = (double)bmpY / 600.0;
				 }else{
					 divisionFactor = (double)bmpX / 600.0;
				 }
				 return (unsigned int)( bmpY / divisionFactor );
			 }
	};
}
