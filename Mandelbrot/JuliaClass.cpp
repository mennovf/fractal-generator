#include "JuliaClass.h"

JuliaClass::JuliaClass(int nMaxIts, double dMaxDist)
	:MandelbrotClass(nMaxIts, dMaxDist)
{
	startComplx->setReal(0.0);
	startComplx->setImag(0.0);
}

JuliaClass::~JuliaClass()
{
	delete complx;
	delete startComplx;
}

//JuliaClass::JuliaClass(JuliaClass% julia){
//	if(julia.complx != nullptr){
//		setReal(julia.complx->Real());
//		setImag(julia.complx->Imag());
//	}
//	maxIts = julia.getMaxIts();
//	maxDist = julia.getMaxDist();
//	autoCalculate = julia.autoCalculates();
//
//	if(julia.startComplx != nullptr){
//		setStartReal(julia.startComplx->Real());
//		setStartImag(julia.startComplx->Imag());
//	}
//}

void JuliaClass::setStartReal(double dReal)
{
	startComplx->setReal(dReal);
}

void JuliaClass::setStartImag(double dImag)
{
	startComplx->setImag(dImag);
}

double JuliaClass::getStartReal()
{
	return startComplx->Real();

}

double JuliaClass::getStartImag()
{
	return startComplx->Imag();
}

int JuliaClass::calculateIts()
{

	while (numIts < maxIts && 
		((complx->Real() * complx->Real()) + (complx->Imag() * complx->Imag())) < (this->maxDist * this->maxDist) )
	{
		complx = (complx->squared())->add(startComplx);

		numIts++;
	}

	int totalNumIts = numIts;
	numIts = 0;
	return totalNumIts;
}