#include "Visuals.h"
#include <iostream>
#pragma warning (disable :4100)


Visuals::Visuals(mine::Program ^ prg)
{
	program = prg;
	colorScheme = 0;
	colorAmount = 1;
	activeColor =1;

	color2Sequential = false;

	reds = gcnew cliext::vector<unsigned short>();
	greens = gcnew cliext::vector<unsigned short>();
	blues = gcnew cliext::vector<unsigned short>();
}

Visuals::~Visuals(){
	delete reds;
	delete greens;
	delete blues;
}

void Visuals::setColorAmount(unsigned short tempAmount){
	colorAmount = tempAmount;

	reds->resize(colorAmount);
	greens->resize(colorAmount);
	blues->resize(colorAmount);
}

void Visuals::setR(unsigned int location, unsigned short value ){
	std::cout << location << ", " << value << std::endl; 
	if (static_cast<int>(location) < reds->size())
		(*reds)[location] = value;
}

void Visuals::setG(unsigned int location, unsigned short value ){
		std::cout << location << ", " << value << std::endl; 
	if (static_cast<int>(location) < greens->size())
		(*greens)[location] = value;
}

void Visuals::setB(unsigned int location, unsigned short value ){
		std::cout << location << ", " << value << std::endl; 
	if (static_cast<int>(location) < blues->size())
		(*blues)[location] = value;
}

void Visuals::setScheme(unsigned short tempInt){
	colorScheme = tempInt;
}
unsigned short Visuals::getScheme(){
	return colorScheme;}

void Visuals::setColor2Sequential(bool tempBool){
	color2Sequential = tempBool;
}


  /***************************************************
	Get windows color Black>Blue>Red>Black
	**************************************************/

System::Drawing::Color Visuals::getColor(int iteration)
{
	switch(colorScheme){
		case 0: return getColor0(iteration); break;
		case 1: return getColor1(iteration); break;
		case 2 : return getColor2(iteration); break;
		default: return getColor0(iteration); break;
	}
}

System::Drawing::Color Visuals::getColor0(int iteration){
	float percentage = (float)iteration / program->mandelbrot->getMaxIts() * 100;

	if(percentage < 25.0)
	{
		float currentPercentage = (percentage / 25);
		return System::Drawing::Color::FromArgb(0,0, static_cast<int>(255*currentPercentage));
	}
	else if (percentage < 50.0)
	{
		float currentPercentage = (percentage - 25) / 25;
		return System::Drawing::Color::FromArgb(static_cast<int>(255*currentPercentage),0,255);
	}
	else if(percentage < 75.0)
	{
		float currentPercentage = (percentage - 50) / 25;
		return System::Drawing::Color::FromArgb(255,0,static_cast<int>(255-(255*currentPercentage)));
	}
	else
	{
		float currentPercentage = (percentage - 75) / 25;
		return System::Drawing::Color::FromArgb(static_cast<int>(255-(255*currentPercentage)),0,0);
	}
}


/*************************************************************
 Get Windows Color for White>Green>Yellow>Black
*************************************************************/

System::Drawing::Color Visuals::getColor1(int iteration)
{
	float percentage = (float)iteration / program->mandelbrot->getMaxIts() * 100;


	if(percentage <= 33.33)
	{
		float currentPercentage = static_cast<float>((percentage - 0.0) / 33.33);
		return System::Drawing::Color::FromArgb(static_cast<int>(255-(255*currentPercentage)), 255, static_cast<int>(255-(255*currentPercentage)));
	}
	else if (percentage <= 66.66)
	{
		float currentPercentage = static_cast<float>((percentage - 33.33) / 33.33);
		return System::Drawing::Color::FromArgb(static_cast<int>(255*currentPercentage),255,0);
	}
	else
	{
		float currentPercentage = static_cast<float>((percentage - 66.66) / 33.33);
		return System::Drawing::Color::FromArgb(static_cast<int>(255-(255*currentPercentage)), static_cast<int>(255-(255*currentPercentage)), 0);
	}

}



/*************************************************************
 Get Custom Color
*************************************************************/
System::Drawing::Color Visuals::getColor2(int iteration){

	switch(color2Sequential){
	case true: return getColor2Sequential(iteration); break;
	default: return getColor2Lineair(iteration); break;
	}
}



System::Drawing::Color Visuals::getColor2Lineair(int iteration){

	float percentage = (float)iteration / program->mandelbrot->getMaxIts() * 100;
	System::Drawing::Color tempColor = System::Drawing::Color::FromArgb(reds[0],greens[0],blues[0]) ;

	for (unsigned int i = 0; i < (unsigned int)(colorAmount-1); i++){
		if( (percentage >=  (i)*(100.0 / colorAmount)) && (percentage <= (i+1)*(100.0 / colorAmount)) ){

			double multiplier = ( (percentage - (i)*(100.0/colorAmount)) / (100.0 / colorAmount) );
			if(multiplier < 0)
				multiplier = 0;

			//double red = the difference between the current color and the next one
			unsigned short reds2 = (*reds)[i+1];
			double red = static_cast<double>(reds2) - static_cast<double>((*reds)[i]);

			//now the difference gets multiplied by the percentage difference of this step and the next one
			//the multiplier can exceed 1 only if this isn't the right step in the loop yet
			red *= multiplier;
			//now the difference gets added the the current known color to get the final result (in a double)
			red += (*reds)[i];

			unsigned short greens2 = (*reds)[i+1];
			double green = static_cast<double>(greens2) - static_cast<double>((*greens)[i]);
			green *= multiplier;
			green += (*greens)[i];

			unsigned short blues2 = (*reds)[i+1];
			double blue = static_cast<double>(blues2) - static_cast<double>((*blues)[i]);
			blue *= multiplier;
			blue += (*blues)[i];

			//Red Caps
			if(red > 255)
				red = 255;
			else if(red < 0)
				red = 0;
			//Green Caps
			if(green > 255)
				green = 255;
			else if(green < 0)
				green = 0;
			//Blue Caps
			if(blue >255)
				blue = 255;
			else if (blue < 0)
				blue = 0;

			tempColor = System::Drawing::Color::FromArgb( static_cast<unsigned int>(red), static_cast<unsigned int>(green), static_cast<unsigned int>(blue));

		}else{break;}
	}
	return tempColor;

}

System::Drawing::Color Visuals::getColor2Sequential(int iteration){
	unsigned int iterationMod = iteration % colorAmount;
	System::Drawing::Color tempColor = System::Drawing::Color::FromArgb((*reds)[0], (*greens)[0], (*blues)[0]);

	for(unsigned int i = 0 ; i < colorAmount ; i++ ){
		if (iterationMod == i)
			tempColor = System::Drawing::Color::FromArgb((*reds)[i], (*greens)[i], (*blues)[i]);
	}
	return tempColor;
}