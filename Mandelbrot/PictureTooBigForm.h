#pragma once
#pragma warning(disable : 4100)

namespace Mandelbrot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for PictureTooBigForm
	/// </summary>
	public ref class PictureTooBigForm : public System::Windows::Forms::Form
	{
	public:
		PictureTooBigForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PictureTooBigForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  lbl_SizeError;
	protected: 
	private: System::Windows::Forms::Button^  btn_OK;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->lbl_SizeError = (gcnew System::Windows::Forms::Label());
			this->btn_OK = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// lbl_SizeError
			// 
			this->lbl_SizeError->AutoSize = true;
			this->lbl_SizeError->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lbl_SizeError->Location = System::Drawing::Point(12, 9);
			this->lbl_SizeError->Name = L"lbl_SizeError";
			this->lbl_SizeError->Size = System::Drawing::Size(222, 48);
			this->lbl_SizeError->TabIndex = 0;
			this->lbl_SizeError->Text = L"The dimension of the \r\nimage are to big too load.";
			// 
			// btn_OK
			// 
			this->btn_OK->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->btn_OK->Location = System::Drawing::Point(71, 75);
			this->btn_OK->Name = L"btn_OK";
			this->btn_OK->Size = System::Drawing::Size(96, 35);
			this->btn_OK->TabIndex = 1;
			this->btn_OK->Text = L"OK";
			this->btn_OK->UseVisualStyleBackColor = true;
			this->btn_OK->Click += gcnew System::EventHandler(this, &PictureTooBigForm::btn_OK_Click);
			// 
			// PictureTooBigForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(265, 122);
			this->Controls->Add(this->btn_OK);
			this->Controls->Add(this->lbl_SizeError);
			this->Name = L"PictureTooBigForm";
			this->Text = L"Size Error";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void btn_OK_Click(System::Object^  sender, System::EventArgs^  e) {
				 Close();
			 }
	};
}
