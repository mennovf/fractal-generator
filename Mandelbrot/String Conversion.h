
//#include <stdlib.h>
//#include <vcclr.h>
//#include <string>
//using namespace System;
//
//bool To_CharStar( String^ source, char*& target )
//{
//    pin_ptr<const wchar_t> wch = PtrToStringChars( source );
//    int len = (( source->Length+1) * 2);
//    target = new char[ len ];
//    return wcstombs( target, wch, len ) != -1;
//}
// 
//bool To_string( String^ source, std::string * target )
//{
//    pin_ptr<const wchar_t> wch = PtrToStringChars( source );
//    int len = (( source->Length+1) * 2);
//    char *ch = new char[ len ];
//    bool result = wcstombs( ch, wch, len ) != -1;
//    *target = ch;
//    delete ch;
//    return result;
//}
//
//char* toCharStr(System::String^ source)
//{
//	String ^str = source;
//
//   // Pin memory so GC can't move it while native function is called
//   pin_ptr<const wchar_t> wch = PtrToStringChars(str);
//
//   // Conversion to char* :
//   // Can just convert wchar_t* to char* using one of the 
//   // conversion functions such as: 
//   // WideCharToMultiByte()
//   // wcstombs_s()
//   // ... etc
//   size_t convertedChars = 0;
//   size_t  sizeInBytes = ((str->Length + 1) * 2);
//   errno_t err = 0;
//   char    *ch = (char *)malloc(sizeInBytes);
//
//   err = wcstombs_s(&convertedChars, 
//                    ch, sizeInBytes,
//                    wch, sizeInBytes);
//   if (err != 0) return nullptr;
//      
//   else return ch;
//}