#include <cmath>
#include "mandelbrotClass.h"

MandelbrotClass::MandelbrotClass(int nMaxIts, double dMaxDist)
{
	maxIts = nMaxIts;
	maxDist = dMaxDist;
	complx = gcnew Complex(0.0, 0.0);
	startComplx = gcnew Complex(0.0, 0.0);
	numIts = 0;

}

MandelbrotClass::~MandelbrotClass()
{
	delete complx;
	complx = nullptr;
	delete startComplx;
	startComplx = nullptr;
}
//Copy constructor
MandelbrotClass::MandelbrotClass(MandelbrotClass% brot){
	if(brot.complx != nullptr){
		setReal(brot.complx->Real());
		setImag(brot.complx->Imag());
	}
	maxIts = brot.getMaxIts();
	maxDist = brot.getMaxDist();

	if(brot.startComplx != nullptr){
		startComplx->setReal(brot.startComplx->Real());
		startComplx->setImag(brot.startComplx->Imag());
	}
}

//Recursive function which for some reason decided not to work although it should

//int MandelbrotClass::calculateIts()
//{
//
//	//static int because numIts shouldn't be reinitialized to 0 each time it's being called (get initialised to 0 automatically because it's a static int)
//	if (numIts == 0)
//	{
//		startComplx->setReal(complx->Real());
//		startComplx->setImag(complx->Imag());
//	}
//
//	//double distance = sqrt((complx->Real() * complx->Real()) + (complx->Imag() * complx->Imag()));
//
//	//Recursive function conditions: checks wether the max number of its is reached or the max distance to the origin
//	//Distance is the square root of the sum of the 2 quadratic values but instead of doing the square root we compare it to the squared of the usual comparer 
//	if (numIts < maxIts && 
//		((complx->Real() * complx->Real()) + (complx->Imag() * complx->Imag())) < (maxDist * maxDist) )
//	{
//		//Mandelbrot!
//		complx = (complx->squared())->add(startComplx);
//
//		numIts++;
//		calculateIts();
//	}
//	else
//	{
//		int totalNumIts = numIts;
//		numIts = 0;
//		return totalNumIts;
//	}
//
//	
//
//}

int MandelbrotClass::calculateIts()
{

	if (numIts == 0)
	{
		startComplx->setReal(complx->Real());
		startComplx->setImag(complx->Imag());
	}


	while (numIts < maxIts && ((complx->Real() * complx->Real()) + (complx->Imag() * complx->Imag())) < (this->maxDist * this->maxDist) )
	{
		complx = (complx->squared())->add(startComplx);

		numIts++;
	}

	int totalNumIts = numIts;
	iterationsRequired = totalNumIts;
	numIts = 0;
	return totalNumIts;

}

void MandelbrotClass::setReal(double realPart)
{
	Complex^ tempComplx = gcnew Complex(realPart, complx->Imag());
	delete complx;
	complx = tempComplx;
	delete tempComplx;

}

void MandelbrotClass::setImag(double imagPart)
{
	Complex^ tempComplx = gcnew Complex(complx->Real(), imagPart);
	delete complx;
	complx = tempComplx;
	delete tempComplx;

}

void MandelbrotClass::setMaxDist(double tempMaxDist)
{
	maxDist = tempMaxDist;
}

void MandelbrotClass::setMaxIts(int tempMaxIts)
{
	maxIts = tempMaxIts;
	
}

int MandelbrotClass::getMaxIts()
{
	return maxIts;
}