//#include <Afx.h>
#include "Program.h"
#include "Visuals.h"
#include "buddhabrotClass.h"
#include <vcclr.h>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cmath>


//Macro's
#define DIVISION_FACTOR 500
#define ZOOM_DIVISION 700
#define BMP_SIZE_X 1500
#define BMP_SIZE_Y 1500
#define ORIGIN_X_PERCENT 75.0
#define ORIGIN_Y_PERCENT 50.0
#define MAX_ITS 50
#define MAX_DISTANCE 2.0
#define FRACTAL_TYPE MANDELBROT
#define SAMPLES 10000000

using namespace mine;


//Constructor
Program::Program(System::Windows::Forms::ProgressBar^ tempBar, System::ComponentModel::BackgroundWorker^ TempBGWorker)
{
	BMPSizeX = BMP_SIZE_X;
	BMPSizeY = BMP_SIZE_Y;
	BMPSurface = BMPSizeX * BMPSizeY;

	zoomDivision = ZOOM_DIVISION; 

	filePath = gcnew System::String("0");

	originX = BMP_SIZE_X /100 * ORIGIN_X_PERCENT;
	originY = BMP_SIZE_Y /100 * ORIGIN_Y_PERCENT;


	//Classes
	mandelbrot = gcnew MandelbrotClass(MAX_ITS, MAX_DISTANCE);
	julia = gcnew JuliaClass(MAX_ITS, MAX_DISTANCE);
	buddha = gcnew BuddhabrotClass(this, SAMPLES, MAX_ITS, MAX_DISTANCE);
	m_visuals = gcnew Visuals(this);

	//Bitmap
	mandelbrotBMP = gcnew System::Drawing::Bitmap(BMP_SIZE_X, BMP_SIZE_Y, System::Drawing::Imaging::PixelFormat::Format32bppArgb);

	//pBar = tempBar;
	pBar= tempBar;

	BGWorker = TempBGWorker;
	progress = 0;

	fractalType = FRACTAL_TYPE;
	fileType = 1;

	elapsedTime = 0.0;

	xToTrace = 0;
	yToTrace = 0;
}


Program::~Program()
{
	delete mandelbrot;
	delete m_visuals;
	delete filePath;
	delete mandelbrotBMP;
}

void Program::setBMPSize(int x, int y)
{
	if(x != 0)
		BMPSizeX = x;
	if(y != 0)
		BMPSizeY = y;
	BMPSurface = BMPSizeX * BMPSizeY;
}

void Program::setBMPSizeX(unsigned int x)
{
	if (x != 0)
		BMPSizeX = x;
	BMPSurface = BMPSizeX * BMPSizeY;

}

void Program::setBMPSizeY(unsigned int y)
{
	if(y != 0)
		BMPSizeY = y;
	BMPSurface = BMPSizeX * BMPSizeY;
}

unsigned int Program::getBMPX()
{
	return BMPSizeX;
}

unsigned int Program::getBMPY()
{
	return BMPSizeY;
}

void Program::setPath(System::String^ tempString)
{
	filePath = tempString;
}

System::String^ Program::getPath()
{
	return filePath;
}

void Program::setOriginX(double x)
{
	originX = BMPSizeX / 100 * x;
}

void Program::setOriginY(double y)
{
	originY = BMPSizeY / 100 * y;
}

double Program::getOriginX(){
	return originX;
}

double Program::getOriginY(){
	return originY;
}

void Program::setZoomDivision(double tempDiv)
{
	zoomDivision = tempDiv * 1000;
}

double Program::getZoomDivision()
{
	return zoomDivision;
}

void Program::setFType(short tempShort){
	fractalType = tempShort;
}

short Program::getFType(){
	return fractalType;
}

double Program::getTime(){
	return elapsedTime;
}

void Program::setTime(double tempTime){
	elapsedTime = tempTime;
}

void Program::addTime(double timeAdd){
	elapsedTime += timeAdd;
}

bool Program::isMSet(){
	mandelbrot->setReal(julia->getStartReal());
	mandelbrot->setImag(julia->getStartImag());

	if (mandelbrot->calculateIts() == mandelbrot->getMaxIts())
		return true;
	else 
		return false;
}

void Program::setFileType(short tempType){
	fileType = tempType;
}

short Program::getFileType(){
	return fileType;
}


double Program::getCoordX(int x)
{
	return ((double)x - originX);

}

double Program::getCoordY(int y)
{
	return (originY - (double)y);
}

void Program::setColorScheme(unsigned short tempShort){
	m_visuals->setScheme(tempShort);
}


void Program::pBarUpdate(int x, int y)
{

	//If the current pixel they are working on a multiplication of the total divided by 20 is, the pBar will update
	//ex. 20 % 20 = 0, 40 % 20 = 0 and so on...
	if (((x+1)*(y+1))%(BMPSurface/20) == 0)
	{
		if(pBar->Value < 90)
		{
			progress += 5;
			BGWorker->ReportProgress(progress);
		}
	}
}

int xCarthToPixel(double carth, Program^ prg){
	return static_cast<int>(carth + prg->getOriginX());
}

int yCarthToPixel(double carth, Program^ prg){
	return static_cast<int>(prg->getOriginY() - carth);
}

void Program::drawCircle(int x, int y, bool drawCorners, System::Drawing::Color color){
	mandelbrotBMP->SetPixel(x,y, color);
	mandelbrotBMP->SetPixel(x+1,y, color);
	mandelbrotBMP->SetPixel(x-1,y, color);
	mandelbrotBMP->SetPixel(x,y+1, color);
	mandelbrotBMP->SetPixel(x,y-1, color);
	
	if (drawCorners == true){
	mandelbrotBMP->SetPixel(x+1,y+1, color);
	mandelbrotBMP->SetPixel(x+1,y-1, color);
	mandelbrotBMP->SetPixel(x-1,y+1, color);
	mandelbrotBMP->SetPixel(x-1,y-1, color);
	}
}

double Program::distance(double x1, double y1, double x2, double y2){
	return sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));
}

//void Program::pixelLooper(){
//
//	System::Drawing::Color green = System::Drawing::Color::FromArgb(0,255,0);
//	System::Drawing::Color red = System::Drawing::Color::FromArgb(255,0,0); 
//
//	Complex^ startComplx = gcnew Complex(xToTrace, yToTrace);
//	Complex^ complx = gcnew Complex(xToTrace, yToTrace);
//	double prevComplxX = 0;
//	double prevComplxY = 0;
//
//	for(unsigned int i = 0 ; i < static_cast<unsigned int>(mandelbrot->getMaxIts()) ; i++){
//	
//
//		int xPixel = xCarthToPixel(complx->Real()*zoomDivision, this);
//		int yPixel = yCarthToPixel(complx->Imag()*zoomDivision, this);
//
//		drawCircle(xPixel, yPixel, true, green);
//
//		prevComplxX = complx->Real();
//		prevComplxY = complx->Imag();
//		complx = (complx->squared())->add(startComplx);
//
//		//Distance
//		double distanceBetweenPoints = distance(prevComplxX, prevComplxY, complx->Real(), complx->Imag());
//		//Normalized vectors
//		double xNormalVec = (prevComplxX - complx->Real()) / distanceBetweenPoints;
//		double yNormalVec = (prevComplxY - complx->Imag()) / distanceBetweenPoints;
//		//Start vectors
//		double currentX = (prevComplxX);
//		double currentY = (prevComplxY);
//
//		//i = 1 and i < .. ,not <= because I don't want to recolour the dots
//		for(int i = 1 ; i < static_cast<int>(distanceBetweenPoints) ; i++){
//			//Make sure the new pixel isn"t out of the bitmap range
//			if((currentX + (i * xNormalVec)) < static_cast<double>(BMPSizeX)
//				&& (currentX + (i * xNormalVec)) >= 0.0 
//				&& (currentY + (i * yNormalVec)) < static_cast<double>(BMPSizeY) 
//				&& (currentY + (i * yNormalVec)) >= 0.0){
//					drawCircle(static_cast<int>(currentX + (i * xNormalVec)), static_cast<int>(currentY + (i * yNormalVec)), false, red);
//			}
//		}
//
//	}
//
//
//}





void Program::pixelLooper()
{
	if (fractalType != 2)
	{
		for(unsigned int x = 0; x < BMPSizeX; x++)
		{
			for (unsigned int y = 0; y < BMPSizeY; y++)
			{
				double dx = getCoordX(x)/zoomDivision;
				double dy = getCoordY(y)/zoomDivision;

				System::Drawing::Color tempColor = System::Drawing::Color::FromArgb(0,0,0);



				if(fractalType == 0)
				{
					mandelbrot->setReal(dx);
					mandelbrot->setImag(dy);

					tempColor = m_visuals->getColor(mandelbrot->calculateIts());
				}

				if (fractalType == 1)
				{
					julia->setReal(dx);
					julia->setImag(dy);

					tempColor = m_visuals->getColor(julia->calculateIts());
				}


				mandelbrotBMP->SetPixel(x,y,tempColor);
			
				pBarUpdate(x,y);

			}
		}
	}

	//if fractaltype == 2
	else
	{
		buddha->setSeed(static_cast<unsigned int>(time(0)));

		std::cout << buddha->getSeed() << std::endl;

		double x0 = getCoordX(0)/zoomDivision;
		double y0 = getCoordY(0)/zoomDivision;
		double xMax = getCoordX(getBMPX())/zoomDivision;
		double yMax = getCoordY(getBMPY())/zoomDivision;

		double minimum = 0;
		double maximum = 0;

		//Make minimum the lowest value
		if (x0 < y0){
			if (xMax < yMax)
				minimum = (x0 < xMax) ? x0 : xMax;
			else 
				minimum = (x0 < yMax) ? x0 : yMax;
		}
		else {
			if (xMax < yMax)
				minimum = (y0 < xMax) ? y0 : xMax;
			else 
				minimum = (y0 < yMax) ? y0 : yMax;
		}

		//Make maximum the highest value
		if (x0 > y0){
			if (xMax > yMax)
				maximum = (x0 > xMax) ? x0 : xMax;
			else 
				maximum = (x0 > yMax) ? x0 : yMax;
		}
		else {
			if (xMax > yMax)
				maximum = (y0 > xMax) ? y0 : xMax;
			else 
				maximum = (y0 > yMax) ? y0 : yMax;
		}

		if (minimum < 0)
			minimum += (minimum*2);
		else 
			minimum -= (minimum*2);

		if(maximum < 0)
			maximum -= (maximum*2);
		else 
			maximum += (maximum*2);


		
		//set the minimum and maximum to sample from the boundaries of the bitmap +/- some handles of twice the value
		//because if you sample from an area of the mandelbrot it doesn't get used
		buddha->setMin( minimum );
		buddha->setMax( maximum );

		buddha->buildCache();

		std::cout << "Calculating samples..." << std::endl;

		for (unsigned int i = 1; i <= buddha->getSamples() ; i++)
		{
			
			std::cout << std::endl;
			std::cout << i << "/" << buddha->getSamples() << std::endl;
			std::cout << std::endl;

			//Keep sampling untill it's a random number that's not part of the M-set because it'll always reach the max its
			buddha->random();

			//Re-get a random value if the complex number IS part of the M-set
			while (buddha->getIts() == buddha->getMaxIts())
			{
				buddha->random();
			}

			std::cout << buddha->getReal() << " " << buddha->getImag() << std::endl;

			buddha->calculateAndApply();

			if(buddha->getSamples() >= 20)
			{
				double multiplicationsRequired = static_cast<double>(buddha->getSamples() / 20);
				if (  (i) % (static_cast<unsigned int>(multiplicationsRequired)) == 0 )
				{
						if(pBar->Value < 80)
						{
							progress += 4;
							BGWorker->ReportProgress(progress);
						}//END if statement
				}
			}//END if statement

		}//END for loop

		std::cout << "Done..." << std::endl;

		unsigned int mostVisited = 0;

		std::cout << "Looking for the highest value" << std::endl;

		for (unsigned int x = 0; x < getBMPX() ; x++)
		{
			for (unsigned int y = 0; y < getBMPY() ; y++)
			{
				//store in variable for easy acces
				unsigned int c = buddha->getPixelFromCache(x, y);

				if (c > mostVisited)
				{
					mostVisited = c;

				}


			}
		}

		std::cout << "Done...\n" << "Highest Integer: " << mostVisited  << "\nUpdating bitmap..." << std::endl;

		for (unsigned int x = 0; x < getBMPX() ; x++)
		{
			//std::cout << "Doing " << (x+1) << "nd row..." << std::endl;

			for (unsigned int y = 0; y < getBMPY() ; y++)
			{

				//store in variable for easy acces
				//Make tha value a percentage of the biggest number
				unsigned int c = buddha->getPixelFromCache(x, y);

				double cPercentage = static_cast<double>(c)/static_cast<double>(mostVisited);
				c = static_cast<unsigned int>(cPercentage * 255);

				//Apply the pixel to the bitmap
				System::Drawing::Color newColor = System::Drawing::Color::FromArgb(c,c,c);
				mandelbrotBMP->SetPixel(x,y, newColor) ;

				if (((x+1)*(y+1))%(BMPSurface/10) == 0)
				{
					if(pBar->Value < 90)
					{
						progress += 3;
						BGWorker->ReportProgress(progress);
					}

				}
			}// end y loop
			//std::cout << std::endl;
		}//end x loop
		std::cout << "Done..." << std::endl;
	
		buddha->clearCache();
		
	}//end else

}





//NEED TO ADD TO THIS
bool Program::run()
{
	startTime = time(0);

	delete mandelbrotBMP;
	mandelbrotBMP = gcnew System::Drawing::Bitmap(BMPSizeX, BMPSizeY, System::Drawing::Imaging::PixelFormat::Format32bppArgb);

	pixelLooper();

	elapsedTime = difftime(time(0), startTime );

	std::cout << "saving bitmap as specified file path..." << std::endl;

	if(mandelbrotBMP != nullptr)
	{
		if(SystemToCharStr(filePath) != nullptr)
		{
			switch(fileType){
			case 0: mandelbrotBMP->Save( filePath, System::Drawing::Imaging::ImageFormat::Bmp ); break;
			case 1: mandelbrotBMP->Save( filePath, System::Drawing::Imaging::ImageFormat::Png ); break;
			case 2: mandelbrotBMP->Save( filePath, System::Drawing::Imaging::ImageFormat::Jpeg ); break;
			}
			std::cout << "bitmap saved..." << std::endl;
		}

		return true;
	}
	else return false;
}


char* Program::SystemToCharStr(System::String^ source)
{
	String ^str = source;

   // Pin memory so GC can't move it while native function is called
   pin_ptr<const wchar_t> wch = PtrToStringChars(str);

   // Conversion to char* :
   // Can just convert wchar_t* to char* using one of the 
   // conversion functions such as: 
   // WideCharToMultiByte()
   // wcstombs_s()
   // ... etc
   size_t convertedChars = 0;
   size_t  sizeInBytes = ((str->Length + 1) * 2);
   errno_t err = 0;
   char    *ch = (char *)malloc(sizeInBytes);

   err = wcstombs_s(&convertedChars, 
                    ch, sizeInBytes,
                    wch, sizeInBytes);
   if (err != 0) return nullptr;
      
   else return ch;
}


