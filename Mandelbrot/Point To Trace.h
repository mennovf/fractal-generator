#pragma once
#include "Program.h"
#pragma warning (disable : 4100)

namespace Mandelbrot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for PointToTrace
	/// </summary>
	public ref class PointToTrace : public System::Windows::Forms::Form
	{
	public:
		PointToTrace(mine::Program^ prog)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			prg = prog;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PointToTrace()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  lbl_x;
	protected: mine::Program^ prg;
	private: System::Windows::Forms::Label^  lbl_y;
	private: System::Windows::Forms::NumericUpDown^  spnr_x;
	private: System::Windows::Forms::NumericUpDown^  spnr_y;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->lbl_x = (gcnew System::Windows::Forms::Label());
			this->lbl_y = (gcnew System::Windows::Forms::Label());
			this->spnr_x = (gcnew System::Windows::Forms::NumericUpDown());
			this->spnr_y = (gcnew System::Windows::Forms::NumericUpDown());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_x))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_y))->BeginInit();
			this->SuspendLayout();
			// 
			// lbl_x
			// 
			this->lbl_x->AutoSize = true;
			this->lbl_x->Location = System::Drawing::Point(25, 28);
			this->lbl_x->Name = L"lbl_x";
			this->lbl_x->Size = System::Drawing::Size(35, 13);
			this->lbl_x->TabIndex = 0;
			this->lbl_x->Text = L"Real: ";
			// 
			// lbl_y
			// 
			this->lbl_y->AutoSize = true;
			this->lbl_y->Location = System::Drawing::Point(193, 28);
			this->lbl_y->Name = L"lbl_y";
			this->lbl_y->Size = System::Drawing::Size(33, 13);
			this->lbl_y->TabIndex = 1;
			this->lbl_y->Text = L"Imag:";
			// 
			// spnr_x
			// 
			this->spnr_x->DecimalPlaces = 4;
			this->spnr_x->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 131072});
			this->spnr_x->Location = System::Drawing::Point(66, 26);
			this->spnr_x->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {999999999, 0, 0, 0});
			this->spnr_x->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {1410065407, 2, 0, System::Int32::MinValue});
			this->spnr_x->Name = L"spnr_x";
			this->spnr_x->Size = System::Drawing::Size(120, 20);
			this->spnr_x->TabIndex = 2;
			this->spnr_x->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_x->ValueChanged += gcnew System::EventHandler(this, &PointToTrace::spnr_x_ValueChanged);
			// 
			// spnr_y
			// 
			this->spnr_y->DecimalPlaces = 4;
			this->spnr_y->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) {1, 0, 0, 131072});
			this->spnr_y->Location = System::Drawing::Point(232, 26);
			this->spnr_y->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) {999999, 0, 0, 0});
			this->spnr_y->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) {9999999, 0, 0, System::Int32::MinValue});
			this->spnr_y->Name = L"spnr_y";
			this->spnr_y->Size = System::Drawing::Size(120, 20);
			this->spnr_y->TabIndex = 3;
			this->spnr_y->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->spnr_y->ValueChanged += gcnew System::EventHandler(this, &PointToTrace::spnr_y_ValueChanged);
			// 
			// PointToTrace
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(375, 67);
			this->Controls->Add(this->spnr_y);
			this->Controls->Add(this->spnr_x);
			this->Controls->Add(this->lbl_y);
			this->Controls->Add(this->lbl_x);
			this->Name = L"PointToTrace";
			this->Text = L"PointToTrace";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_x))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_y))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void spnr_x_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 prg->setXToTrace((double)spnr_x->Value);
			 }
private: System::Void numericUpDown2_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 prg->setYToTrace((double)spnr_y->Value);
		 }
private: System::Void spnr_y_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 prg->setYToTrace((double)spnr_y->Value);
		 }
};
}
