#ifndef PROGRAM_H
#define PROGRAM_H

#include "mandelbrotClass.h"
#include "buddhabrotClass.h"
#include "JuliaClass.h"
#include <ctime>

ref class Visuals;

namespace mine
{

	//Gebruik ik eigenlijk niet
	/*ref struct vector
	{
		double x;
		double y;
		void vector::operator =(const vector ^tempVec) {x = tempVec->x; y = tempVec->y;}
	};*/
	enum fractalTypes{
		MANDELBROT,
		JULIASET,
		BUDDHABROT
	};

	public ref class Program
	{

	public:
		//Constructor and deconstructor
		Program(System::Windows::Forms::ProgressBar^ tempBar, System::ComponentModel::BackgroundWorker^ TempBGWorker);
		~Program();

		//Bundles the functions together that are needed in the end
		bool run();

		//BMP's variables accesor functions
		void setBMPSize(int x, int y);
		void setBMPSizeX(unsigned int x);
		void setBMPSizeY(unsigned int y);
		unsigned int getBMPX();
		unsigned int getBMPY();

		//Path's accesor functions
		void setPath(System::String^ string);
		System::String^ getPath();
		
		//Origin's accesor functions
		void setOriginX(double x);
		void setOriginY(double y);
		double getOriginX();
		double getOriginY();

		//returns a double value which is the coordinate of the pixel in the origin's raster
		double getCoordX(int x);
		double getCoordY(int y);

		void setZoomDivision(double tempDiv);
		double getZoomDivision();

		void pixelLooper();
		bool isMSet();

		char* SystemToCharStr(System::String^ source);

		System::Drawing::Bitmap^ mandelbrotBMP;

		//mandelbrotclass to calculate the pixel Values
		MandelbrotClass ^ mandelbrot;

		//Julia Class
		JuliaClass^ julia;

		//Buddhabrot Class
		BuddhabrotClass^ buddha;

		//Visuals class for visual things
		Visuals ^ m_visuals;

		//To acces the forms ProgressBar
		System::Windows::Forms::ProgressBar^ pBar;
		void pBarUpdate(int x, int y);

		//Acces to the backgroundworker
		System::ComponentModel::BackgroundWorker^ BGWorker;
		short progress;

		//Accesor functions for elapsedTime
		double getTime();
		void setTime(double tempTime);
		void addTime(double timeAdd);

		//set/get the fractal type
		void setFType(short tempShort);
		short getFType();

		void setFileType(short tempType);
		short getFileType();

		void setColorScheme(unsigned short tempShort);

		void setXToTrace(double x){xToTrace = x;};
		void setYToTrace(double y){yToTrace = y;};





	protected:


		//Which Fractal
		//0 = Mandelbrot Set
		//1 = Julia Set
		//2 = buddhabrot
		short fractalType;

		//0:bmp, 1:png, 2:jpeg
		short fileType;

		//BMP resolution
		unsigned int BMPSizeX;
		unsigned int BMPSizeY;
		unsigned int BMPSurface;

		//Zoom
		double zoomDivision;

		//BMP ssave location
		System::String^ filePath;

		//Origin coords
		double originX;
		double originY;

		time_t startTime;
		time_t endTime;
		double elapsedTime;

		//bool saveImageAs(System::String^ savePath, int fileFormat);

		double xToTrace;
		double yToTrace;
		void drawCircle(int x, int y, bool drawCorners, System::Drawing::Color color);
		double distance(double x1, double y1, double x2, double y2);


	};
}
#endif