#ifndef BUDDHABROTCLASS_H
#define BUDDHABROTCLASS_H

#include "mandelbrotClass.h"
#include "complex.h"


//Pre definitions
namespace mine
{
ref class Program;
}



public ref class BuddhabrotClass : public MandelbrotClass
{
public:
	BuddhabrotClass(mine::Program^ tempSender, unsigned int tempSamples, int maxIts, double maxDist);
	~BuddhabrotClass();
	void calculateAndApply();

	void setMax(double max);
	void setMin(double min);
	
	void setSamples(unsigned int tempSamples);
	unsigned int getSamples();

	void buildCache();
	void clearCache();
	unsigned int getPixelFromCache(unsigned int x, unsigned int y);

	void random();
protected:
	unsigned int pixelValueX(double x);
	unsigned int pixelValueY(double y);
	unsigned int toPixel(double xy);

	void iterate(bool firstIt);
	Complex^ iteratedComplx;

	unsigned int samples;

	//Has to point to a 2-dimensional array, no idea how to acclompish this yet
	unsigned int ** pixelCache;



private:
		mine::Program^ sender;
};


#endif