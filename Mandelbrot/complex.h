#ifndef COMPLEX_H
#define COMPLEX_H
#include "mtrand.h"

using namespace System;


	public  ref  class Complex
	{
	public:
	Complex();
	Complex(double real, double imag);
	~Complex();
	Complex(Complex% complx);


	/*********
    *  Operators
	*********/
	/*static Complex^ operator+ (const Complex^ tempComp2);
	static Complex^ operator* (const Complex^ c1, const Complex^ c2);
*/

	/*******
	* Methods
	*********/
	
	//Mathematical operations
	Complex^ add(Complex^ tempComp);
	Complex^ squared();

	//Accessor functions
	double Real() { return _real; }
	double Imag() { return _imag; }
	void setReal(double tempReal) {_real = tempReal;}
	void setImag(double tempImag) {_imag = tempImag;}

	void randReal() {_real = PRNG->gen();}
	void randImag() {_imag = PRNG->gen();}
	void random() {randReal(); randImag();}

	void setMin(double min);
	void setMax(double max);
	double getMin(){return PRNG->getMin();}
	double getMax(){return PRNG->getMax();}

	unsigned int getSeed() {return randSeed;}
	void setSeed(unsigned int seed) {randSeed = seed;} 

	private:

	double _real;
	double _imag;
	unsigned long randSeed;
	MTRand_int32 * PRNG;

	};

#endif

