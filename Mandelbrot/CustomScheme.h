#pragma once
#pragma warning (disable : 4100)
#include "Program.h"
#include "visuals.h"
#include <iostream>

namespace Mandelbrot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for CustomScheme
	/// </summary>
	public ref class CustomScheme : public System::Windows::Forms::Form
	{
	public:
		CustomScheme(mine::Program^ tempProg)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			color = gcnew System::Drawing::Color;
			program = tempProg;
			rdioBtn_lineair->Checked = true;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~CustomScheme()
		{
			if (components)
			{
				delete components;
			}
		}
	protected: System::Drawing::Color^ color;
	private: System::Windows::Forms::Label^  lbl_colorAmount;
	protected: mine::Program^ program;

	private: System::Windows::Forms::Label^  lbl_activeColor;
	private: System::Windows::Forms::NumericUpDown^  spnr_colorAmount;
	private: System::Windows::Forms::NumericUpDown^  spnr_activeColor;
	private: System::Windows::Forms::ColorDialog^  clrDialog01;
	private: System::Windows::Forms::Button^  btn_selectColor;
	private: System::Windows::Forms::RadioButton^  rdioBtn_lineair;
	private: System::Windows::Forms::RadioButton^  rdioBtn_sequential;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->lbl_colorAmount = (gcnew System::Windows::Forms::Label());
			this->lbl_activeColor = (gcnew System::Windows::Forms::Label());
			this->spnr_colorAmount = (gcnew System::Windows::Forms::NumericUpDown());
			this->spnr_activeColor = (gcnew System::Windows::Forms::NumericUpDown());
			this->clrDialog01 = (gcnew System::Windows::Forms::ColorDialog());
			this->btn_selectColor = (gcnew System::Windows::Forms::Button());
			this->rdioBtn_lineair = (gcnew System::Windows::Forms::RadioButton());
			this->rdioBtn_sequential = (gcnew System::Windows::Forms::RadioButton());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_colorAmount))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_activeColor))->BeginInit();
			this->SuspendLayout();
			// 
			// lbl_colorAmount
			// 
			this->lbl_colorAmount->AutoSize = true;
			this->lbl_colorAmount->Location = System::Drawing::Point(13, 13);
			this->lbl_colorAmount->Name = L"lbl_colorAmount";
			this->lbl_colorAmount->Size = System::Drawing::Size(95, 13);
			this->lbl_colorAmount->TabIndex = 0;
			this->lbl_colorAmount->Text = L"Amount of colours:";
			// 
			// lbl_activeColor
			// 
			this->lbl_activeColor->AutoSize = true;
			this->lbl_activeColor->Location = System::Drawing::Point(13, 42);
			this->lbl_activeColor->Name = L"lbl_activeColor";
			this->lbl_activeColor->Size = System::Drawing::Size(75, 13);
			this->lbl_activeColor->TabIndex = 1;
			this->lbl_activeColor->Text = L"Active colour: ";
			// 
			// spnr_colorAmount
			// 
			this->spnr_colorAmount->Location = System::Drawing::Point(148, 13);
			this->spnr_colorAmount->Name = L"spnr_colorAmount";
			this->spnr_colorAmount->Size = System::Drawing::Size(55, 20);
			this->spnr_colorAmount->TabIndex = 2;
			this->spnr_colorAmount->ValueChanged += gcnew System::EventHandler(this, &CustomScheme::spnr_colorAmount_ValueChanged);
			// 
			// spnr_activeColor
			// 
			this->spnr_activeColor->Location = System::Drawing::Point(148, 40);
			this->spnr_activeColor->Name = L"spnr_activeColor";
			this->spnr_activeColor->Size = System::Drawing::Size(55, 20);
			this->spnr_activeColor->TabIndex = 3;
			this->spnr_activeColor->ValueChanged += gcnew System::EventHandler(this, &CustomScheme::spnr_activeColor_ValueChanged);
			// 
			// clrDialog01
			// 
			this->clrDialog01->AnyColor = true;
			this->clrDialog01->FullOpen = true;
			// 
			// btn_selectColor
			// 
			this->btn_selectColor->Location = System::Drawing::Point(70, 99);
			this->btn_selectColor->Name = L"btn_selectColor";
			this->btn_selectColor->Size = System::Drawing::Size(75, 23);
			this->btn_selectColor->TabIndex = 4;
			this->btn_selectColor->Text = L"Select color";
			this->btn_selectColor->UseVisualStyleBackColor = true;
			this->btn_selectColor->Click += gcnew System::EventHandler(this, &CustomScheme::btn_selectColor_Click);
			// 
			// rdioBtn_lineair
			// 
			this->rdioBtn_lineair->AutoSize = true;
			this->rdioBtn_lineair->Location = System::Drawing::Point(16, 74);
			this->rdioBtn_lineair->Name = L"rdioBtn_lineair";
			this->rdioBtn_lineair->Size = System::Drawing::Size(56, 17);
			this->rdioBtn_lineair->TabIndex = 5;
			this->rdioBtn_lineair->TabStop = true;
			this->rdioBtn_lineair->Text = L"Lineair";
			this->rdioBtn_lineair->UseVisualStyleBackColor = true;
			this->rdioBtn_lineair->CheckedChanged += gcnew System::EventHandler(this, &CustomScheme::rdioBtn_lineair_CheckedChanged);
			// 
			// rdioBtn_sequential
			// 
			this->rdioBtn_sequential->AutoSize = true;
			this->rdioBtn_sequential->Location = System::Drawing::Point(118, 74);
			this->rdioBtn_sequential->Name = L"rdioBtn_sequential";
			this->rdioBtn_sequential->Size = System::Drawing::Size(75, 17);
			this->rdioBtn_sequential->TabIndex = 6;
			this->rdioBtn_sequential->TabStop = true;
			this->rdioBtn_sequential->Text = L"Sequential";
			this->rdioBtn_sequential->UseVisualStyleBackColor = true;
			this->rdioBtn_sequential->CheckedChanged += gcnew System::EventHandler(this, &CustomScheme::rdioBtn_sequential_CheckedChanged);
			// 
			// CustomScheme
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(228, 130);
			this->Controls->Add(this->rdioBtn_sequential);
			this->Controls->Add(this->rdioBtn_lineair);
			this->Controls->Add(this->btn_selectColor);
			this->Controls->Add(this->spnr_activeColor);
			this->Controls->Add(this->spnr_colorAmount);
			this->Controls->Add(this->lbl_activeColor);
			this->Controls->Add(this->lbl_colorAmount);
			this->Name = L"CustomScheme";
			this->Text = L"Custom Colour Scheme";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_colorAmount))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->spnr_activeColor))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void spnr_colorAmount_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (spnr_activeColor->Value > spnr_colorAmount->Value - 1)
					 spnr_activeColor->Value = spnr_colorAmount->Value;

				 spnr_activeColor->Maximum = spnr_colorAmount->Value - 1;
				 program->m_visuals->setColorAmount((unsigned short)spnr_colorAmount->Value);
			 }
	private: System::Void btn_selectColor_Click(System::Object^  sender, System::EventArgs^  e) {
				 if(clrDialog01->ShowDialog() == ::System::Windows::Forms::DialogResult::OK){
					 color = clrDialog01->Color;
					 program->m_visuals->setR((unsigned int)spnr_activeColor->Value, (unsigned short)color->R);
					 program->m_visuals->setG((unsigned int)spnr_activeColor->Value, (unsigned short)color->G);
					 program->m_visuals->setB((unsigned int)spnr_activeColor->Value, (unsigned short)color->B);
				 }
			 }
private: System::Void spnr_activeColor_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 program->m_visuals->setActiveColor((unsigned short)spnr_activeColor->Value);
		 }


private: System::Void rdioBtn_lineair_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			  if(rdioBtn_lineair->Checked == true)
				 program->m_visuals->setColor2Sequential(false);
		 }
private: System::Void rdioBtn_sequential_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(rdioBtn_sequential->Checked == true)
				program->m_visuals->setColor2Sequential(true);
		 }
};
}
